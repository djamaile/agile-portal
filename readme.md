Project starten: 
- open in terminal 
- yarn install 
- geen npm gebruiken 

Git Commands: 
git init 
git status
git add
git commit
git log

Webpack commands: 
yarn build:dev - Bouwt project voor development
yarn build:prid - Bouwt project voor productie (duurt wat langer)
yarn run dev-server - Run project lokaal (localhost:8080)
yarn run start - Run project client side (localhost: 3000) //api functies gebruiken

Jest commands: 
yarn run test  - alle testen worden gerun 
yarn run test -- --watch - run de testen in watch mode
