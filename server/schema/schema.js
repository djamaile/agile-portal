const graphql = require('graphql');
const pgp = require('pg-promise')();
const db = {}
var cn = {
    host: '35.185.45.117', // server name or IP address;
    port: 5432,
    database: 'agile',
    user: 'postgres',
    password: 'admin'
};

db.conn = pgp(cn);

const {
    GraphQLObjectType,
    GraphQLID,
    GraphQLString,
    GraphQLSchema,
    GraphQLList
} = graphql;


const TeamlidType = new GraphQLObjectType({
    name: 'Teamlid',
    fields: {
        id: { type: GraphQLID },
        email: { type: GraphQLString },
        naam: { type: GraphQLString },
        aanvraag_id: {type: GraphQLID},
        project_id: {type: GraphQLID},
        adminrechten: { type: GraphQLString}
    }
})

const ProjectType = new GraphQLObjectType({
    name: 'Project',
    fields:{
        id: {type: GraphQLID},
        naam: { type: GraphQLString },
        type: { type: GraphQLString },
        lead_naam: { type: GraphQLString },
        lead_email: { type: GraphQLString },
        aanvraag_id: {type: GraphQLID},
        teamlid:{
            type: GraphQLList(TeamlidType),
            resolve(parentValue, args){
                const query = `SELECT * FROM teamlid WHERE project_id=${parentValue.id}`;
                return db.conn.many(query)
                .then(data => {
                   return data;
                })
                .catch(err => {
                   return 'The error is', err;
                });
            }
        }
    }
})

const AanvraagType = new GraphQLObjectType({
    name: 'Aanvraag', 
    fields:{
        id: {type: GraphQLID},
        naam: { type: GraphQLString },
        email: { type: GraphQLString },
        divisie: { type: GraphQLString },
        afdeling: { type: GraphQLString },
        team: { type: GraphQLString },
        status: { type: GraphQLString },
        datum: { type: GraphQLString },
        verwerkt_door: { type: GraphQLString },
        verwerkt_op: { type: GraphQLString },
        project:{
            type: ProjectType,
            resolve(parentValue, args){
                const query = `SELECT * FROM project WHERE aanvraag_id=${parentValue.id}`;
                return db.conn.one(query)
                .then(data => {
                   return data;
                })
                .catch(err => {
                   return 'The error is', err;
                });
            }
        }
    }
})

const RootQuery = new GraphQLObjectType({
    name: 'RootQueryType',
    fields: {
        teamlid: {
            type: TeamlidType,
            args: { id: { type: GraphQLID } },
            resolve(parentValue, args) {
                const query = `SELECT * FROM teamlid WHERE id=${args.id}`;
                return db.conn.one(query)
                    .then(data => {
                        return data;
                    })
                    .catch(err => {
                        return 'The error is', err;
                    });
            }
        },
        aanvraag:{
            type: AanvraagType, 
            args:{id: {type: GraphQLID}},
            resolve(parentValue, args){
                const query = `SELECT * FROM aanvraag where id=${args.id}`;
                return db.conn.one(query)
                    .then(data => {
                        return data;
                    })
                    .catch(err => {
                        return 'Error is: ', err
                    });
            }
        },
        project:{
            type: ProjectType, 
            args:{id: {type: GraphQLID}},
            resolve(parentValue, args){
                const query = `SELECT * FROM project where id=${args.id}`;
                return db.conn.one(query)
                    .then(data => {
                        return data;
                    })
                    .catch(err => {
                        return 'Error is: ', err
                    });
            }
        },
        aanvragen:{
            type: GraphQLList(AanvraagType),
            resolve(){
                const query = 'SELECT * from aanvraag ORDER BY datum DESC';
                return db.conn.many(query)
                    .then(data => {
                        return data;
                    })
                    .catch(err => {
                        return 'Error is: ', err
                    });
            }
        }
    }
})


const mutation = new GraphQLObjectType({
    name: 'Mutation',
    fields: {
        addAanvraag:{
            type: AanvraagType, 
            args:{
                id: {type: GraphQLID},
                naam: { type: GraphQLString },
                email: { type: GraphQLString },
                divisie: { type: GraphQLString },
                afdeling: { type: GraphQLString },
                team: { type: GraphQLString },
                status: {type: GraphQLString},
                datum: {type: GraphQLString}
            },
            resolve(parentValue, {id,naam, email, divisie, afdeling,team,status,datum}){
            const query = `INSERT INTO aanvraag (id,naam,email,divisie,afdeling,team,status,datum) VALUES (${id}, '${naam}', '${email}', '${divisie}', '${afdeling}', '${team}', '${status}', '${datum}')`;
            return db.conn.one(query)
                    .then(data => {
                        return data;
                    })
                    .catch(error => {
                        return 'Error is: ', error
                    })
            }
        },
        addProject:{
            type:ProjectType,
            args:{
                id: {type: GraphQLID},
                naam: { type: GraphQLString },
                type: { type: GraphQLString },
                lead_naam: { type: GraphQLString },
                lead_email: { type: GraphQLString },
                aanvraag_id: {type: GraphQLID}
            },
            resolve(parentValue, {id, naam, type, lead_naam, lead_email, aanvraag_id}){
                const query = `INSERT INTO project (id,naam,type,lead_naam,lead_email,aanvraag_id) VALUES (${id}, '${naam}', '${type}', '${lead_naam}', '${lead_email}', ${aanvraag_id})`;
                return db.conn.one(query)
                        .then(data => {
                            return data;
                        })
                        .catch(error => {
                            return 'Error is: ', error
                        })
            }
        },
        addTeamlid:{
            type:TeamlidType,
            args:{
                id: { type: GraphQLID },
                email: { type: GraphQLString },
                naam: { type: GraphQLString },
                aanvraag_id: {type: GraphQLID},
                project_id: {type: GraphQLID},
                adminrechten: { type: GraphQLString}
            },
            
            resolve(parentValue, {id, naam, email, aanvraag_id, project_id, adminrechten}){
                const query = `INSERT INTO teamlid (id,naam,email,aanvraag_id,project_id,adminrechten) VALUES (${id}, '${naam}', '${email}',${aanvraag_id}, ${project_id},'${adminrechten}')`;
                return db.conn.one(query)
                        .then(data => {
                            return data;
                        })
                        .catch(error => {
                            return 'Error is: ', error
                        })
            }
        },
        editAanvraag:{
            type: AanvraagType,
            args:{
                id: {type: GraphQLID},
                verwerkt_door: { type: GraphQLString },
                verwerkt_op: { type: GraphQLString },
            },
            resolve(parentValue, {id,verwerkt_door,verwerkt_op}){
                const query = `UPDATE aanvraag SET status = 'gesloten', verwerkt_door='${verwerkt_door}', verwerkt_op='${verwerkt_op}' WHERE id = ${id}`;
                return db.conn.one(query)
                    .then(data => {
                        return data;
                    })
                    .catch(error => {
                        return 'Error is: ', error
                    })
            }
        }
    }
});


module.exports = new GraphQLSchema({
    query: RootQuery,
    mutation
})
