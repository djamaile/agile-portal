const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const nodemailer = require('nodemailer');
const superagent = require('superagent');
const app = express();
const axios = require('axios');
const publicpath = path.join(__dirname, '..', 'public');
const port = process.env.PORT || 3000;
const cors = require('cors');
const expressGraphQL = require('express-graphql');
const schema = require('./schema/schema');
const moment = require('moment');
//urls voor api calls
const CLOUD_URL = 'http://35.185.45.117:8080';
const JIRA_ACCEPTATIE = 'https://accjira.rijksweb.nl';
const JIRA_PRODUCTIE = '';
const BASE_URL = CLOUD_URL;

app.use(express.static(publicpath));
app.use(express.static(path.join(publicpath, 'dist', 'styles.css')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use('/graphql', expressGraphQL({
    schema,
    graphiql: true
}));

function setAuthHeader(username, password){
    const encodedString =  Buffer.from(`${username}:${password}`).toString('base64');
    axios.defaults.headers.common['Authorization'] = "Basic " + encodedString;
}


app.post('/api/get-project', (req, res) => {
    
    var json = {
        "naam": "hallo",
        "test": "peter"
    }

    res.json({json});
})

/**
 * Gebruiker wordt aan de applicatie toegevoegd
 */
app.post('/api/add-user-to-application', (req, res) => {
    var username = req.body.user;

    axios.post(`${BASE_URL}/rest/api/2/user/application?username=${username}&applicationKey=jira-software`,{})
    .then(resp => {
        console.log('gebruiker toegevoegd aan application');
        console.log(resp.data);
        var data = resp.data;
        res.json({data});
    })
    .catch(error => {
       console.log("Kan user niet toevoegen aan Jira software.");
       res.end();
    });
})

//persoon krijgt admin rol bij een project
app.post('/api/add-admin-role-to-person', (req, res) => {
    var user = {
        "user" : [`${req.body.user}`]
    };

    var projectkey = req.body.projectkey;
    axios.post(`${BASE_URL}/rest/api/2/project/${projectkey}/role/10101`, user)
    .then(resp => {
        console.log('Admin role toegewezen');
        console.log(resp.data);
        var data = resp.data;
        res.json({data});
    })
    .catch(error => {
        console.log("Kon geen admin rol toewijzen");
        res.end();
    });
});
//groep krijgt een developer rol
app.post('/api/add-role-to-group', (req, res) => {
    var group = {
        "group" : [`${req.body.groupname}`]
    };

    var projectkey = req.body.projectkey;
    axios.post(`${BASE_URL}/rest/api/2/project/${projectkey}/role/10100`, group)
    .then(resp => {
        console.log('role toegewezen');
        var data = resp.data;
        res.json({data})
    })
    .catch(error => {
        console.log("Kon geen role toewijzen aan een groep");
        res.end();
    });
});

//nieuwe project api
app.post('/api/new-project', (req, res) => {

    const projectnaam = req.body.projectnaam;
    const projectlead = req.body.projectlead;
    let projectType = req.body.projectType;    

    //pak een type project
    function getType(){
        if(projectType === 'Scrum'){
            return 'com.pyxis.greenhopper.jira:gh-scrum-template';
        }else{
            return 'com.pyxis.greenhopper.jira:gh-kanban-template';
        }
    }
    
    //pak een random avatarId, op deze manier lijkt niet elke project opelkaar. 
    function getAvatar(){
        var avatarsId = [10324, 10325, 10326, 10327, 10328, 10329, 10330, 10001, 10009, 10010, 10011, 10200, 10201, 10331, 10202, 10332, 10203, 10204, 10333, 10205, 10206, 10207, 10208, 10209, 10210, 10211];
        return avatarsId[avatarsId.length * Math.random() | 0];
    }

    function getProjectKey(){
        /**
         * Begin gedeelte van SO
         * https://stackoverflow.com/questions/25344603/javascript-check-if-value-has-at-least-2-or-more-words
         */
        if(projectnaam.trim().indexOf(' ') != -1){
            return projectnaam.split(/\s/).reduce((response, word) => response+=word.slice(0,1), '').toUpperCase();
        }else{
            return projectnaam.substr(0,2).toString().toUpperCase();
        }
    }

    const project = {
        "key":`${getProjectKey()}`,
        "name": `${projectnaam}`,
        "projectTypeKey": "software",
        "projectTemplateKey": `${getType()}`,
        "description": "Een nieuw project",
        "lead":  `${projectlead}`,
        "url": `http://35.185.45.117:8080/projects/${getProjectKey()}/summary`,
        "assigneeType": "PROJECT_LEAD",
        "avatarId": `${getAvatar()}`
    }


    axios.post(`${BASE_URL}/rest/api/2/project`, project)
    .then(resp => {
        console.log('project gemaakt');
        var data = resp.data;
        res.json({data})
     })
     .catch(error => {
         console.log("Kon geen project aanmaken");
         res.end();
     })

})

//maak gebruiker aan
app.post('/api/new-user', (req, res) => {
    const username = req.body.username;  
    const display = req.body.displayname; 
    const user = {
        "name": `${username}`,
        "password": "abracadabra",
        "emailAddress": `${username}`,
        "displayName": `${display}`,
        "applicationKeys": [
            "jira-core"
        ],
        "notification":"true"
    }
    axios.post(`${BASE_URL}/rest/api/2/user`, user)
        .then(resp => {
            console.log('Nieuwe user aangemaakt');
            res.json({resp});
        })
        .catch(error => {
            res.send(error);
        })
});

//vind gebruiker
app.post('/api/find-user', (req, res) => {
    var gebruikersnaam = req.body.gebruikersnaam;
    superagent
        .get(`${BASE_URL}/rest/api/2/user?username=${gebruikersnaam}`)
        .auth('agile.rijksweb.nl', 'Agile@123!')
        .end((err, response) => {
            if(err){
                res.json({err});
            }else{
                res.json({response});
            }
        })
});

//add gebruiker tot een groep
app.post('/api/add-to-group', (req, res) => {
    var name = req.body.user;
    var groupname = req.body.groupname;

    axios.post(`${BASE_URL}/rest/api/2/group/user?groupname=${groupname}`, {name})
    .then(resp => {
        console.log('user aan groep gevoegd');
        var data = resp.data;
        res.json({data});
     })
     .catch(error => {
         console.log("Kon geen gebruiker toevoegen aan een groep");
         res.end();
     })
});

//login api
app.post('/api/login', (req, res) => {
    
    var login= {
        "username": req.body.username,
        "password": req.body.password
    };

    setAuthHeader(req.body.username, req.body.password);

    axios.post(`${BASE_URL}/rest/auth/1/session`, login)
    .then(resp => {
       res.json({resp});
    })
    .catch(error => {
        res.json({error});
    })
});

app.post('/api/accepteren', (req, res) => {
    nodemailer.createTestAccount((err, account) => {
        const email = req.body.aanvraagInformatie.email;
        const datum = moment().format('D MMM, YYYY');
        const naam = req.body.aanvraagInformatie.naam;
        const divisie = req.body.aanvraagInformatie.divisie;
        const afdeling = req.body.aanvraagInformatie.afdeling;
        const team = req.body.aanvraagInformatie.team;
        const projectnaam = req.body.aanvraagInformatie.projectnaam;
        const pjnaam = req.body.aanvraagInformatie.pjnaam;
        const pjemail = req.body.aanvraagInformatie.pjemail;
        const ptype = req.body.aanvraagInformatie.ptype;
        const projectURL = req.body.aanvraagInformatie.url;
        const teamlidnaam = req.body.aanvraagInformatie.teamlid.map(({naam})=>{return`<tr><td>${naam}</td></tr>`}).join(" ");
        const teamlidemail = req.body.aanvraagInformatie.teamlid.map(({email})=>{return`<tr><td>${email}</td></tr>`}).join(" ");
        const teamlidrechten = req.body.aanvraagInformatie.teamlid.map(({adminrechten})=>{return`<tr><td>${adminrechten}</td></tr>`}).join(" ");

        const text = "Best medewerker...."
        const html = `
        <html>
        <head>
            <style>
                .titel{
                    font-size:25px;
                    color:#39870c;
                }
                .sub-titel{
                    color:#39870c;
                    font-weight:400;
                }
            </style>
        </head>
        <body>
        <p class="sub-titel">${datum}</p>
        <p>${naam}<br/>${divisie}<br/>${afdeling}<br/>${team}</p>
        <p>Beste ${naam},<br/><br/>
        Bedankt voor het aanvragen van een project, uw project is nu succesvol verwerkt.<br/>
        Project URL: ${projectURL},<br/>
        Voor meer informatie of vragen kunt u een e-mail sturen naar agile@minbzk.nl</p>
        <br/>
        <h2 class="titel">Aanvraag gegevens</h2>
        <h4 class="sub-titel">Aanvrager: </h4>
        <div class="email-info">
        <p>Naam: ${naam}</p>
        <p>Email: ${email}</p>
        <p>Divisie: ${divisie}</p>
        <p>Afdeling: ${afdeling}</p>
        <p>Team: ${team}</p>
        <h4 class="sub-titel">Project gegevens</h4>
        <div class="email-info">
            <p>Projectnaam: ${projectnaam}</p>
            <p>Projectlead naam: ${pjnaam}</p>
            <p>Projectlead email: ${pjemail}</p>
            <p>Project type: ${ptype}</p>
            </div>
            <h4 class="sub-titel teamlid-info">Teamleden info</h4>                   
            <table width="50%">
                <tr>
                    <td>
                        <table>
                            <tr><th>Naam: </tr></th>
                            ${teamlidnaam}
                        </table>
                    </td>
                    <td>
                        <table>
                            <tr style="text-align:left;"><th>Email: </tr></th>
                            ${teamlidemail}
                        </table>
                    </td>
                    <td>
                        <table>
                            <tr><th>Adminrechten: </tr></th>
                            ${teamlidrechten}
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        </body>
        </html>
        `;

        const transporter = nodemailer.createTransport({
            host: 'smtp.ethereal.email',
            port: 587,
            auth: {
                user: 'olx5bvx3q4tfqfhb@ethereal.email',
                pass: 'mycEWqhVWjcrS8BPCC'
            }
        });

        let mailOptions = {
            from: '"Agile desk" <agile@minbzk.com>', // sender address
            to: `${email}`, // list of receivers
            subject: 'Jira project aanvraag', // Subject line
            text, // plain text body
            html // html body
        };

        transporter.sendMail(mailOptions, (error, info) => {
            console.log('Message sent: %s', info.messageId);
            // Preview only available when sending through an Ethereal account
            console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
        });

    })
})
//verstuur een afwijzing naar de gebruiker
app.post('/api/weigeren', (req, res) => {
    nodemailer.createTestAccount((err, account) => {
        const email = req.body.aanvraagInformatie.email;
        const afwijzing = req.body.aanvraagInformatie.afwijzing;
        const datum = moment().format('D MMM, YYYY');
        const naam = req.body.aanvraagInformatie.naam;
        const divisie = req.body.aanvraagInformatie.divisie;
        const afdeling = req.body.aanvraagInformatie.afdeling;
        const team = req.body.aanvraagInformatie.team;
        const projectnaam = req.body.aanvraagInformatie.projectnaam;
        const pjnaam = req.body.aanvraagInformatie.pjnaam;
        const pjemail = req.body.aanvraagInformatie.pjemail;
        const ptype = req.body.aanvraagInformatie.ptype;
        const teamlidnaam = req.body.aanvraagInformatie.teamlid.map(({naam})=>{return`<tr><td>${naam}</td></tr>`}).join(" ");
        const teamlidemail = req.body.aanvraagInformatie.teamlid.map(({email})=>{return`<tr><td>${email}</td></tr>`}).join(" ");
        const teamlidrechten = req.body.aanvraagInformatie.teamlid.map(({adminrechten})=>{return`<tr><td>${adminrechten}</td></tr>`}).join(" ");

        const text = "Best medewerker...."
        const html = `
        <html>
        <head>
            <style>
                .titel{
                    font-size:25px;
                    color:#39870c;
                }
                .sub-titel{
                    color:#39870c;
                    font-weight:400;
                }
            </style>
        </head>
        <body>
        <p class="sub-titel">${datum}</p>
        <p>${naam}<br/>${divisie}<br/>${afdeling}<br/>${team}</p>
        <p>Beste ${naam},<br/><br/>
        Bedankt voor het aanvragen van een project, helaas moet ik u mededelen dat uw aanvraag is geweigerd.<br/>
        Reden: ${afwijzing}<br/>
        Voor meer informatie of vragen kunt u een e-mail sturen naar agile@minbzk.nl</p>
        <br/>
        <h2 class="titel">Aanvraag gegevens</h2>
        <h4 class="sub-titel">Aanvrager: </h4>
        <div class="email-info">
        <p>Naam: ${naam}</p>
        <p>Email: ${email}</p>
        <p>Divisie: ${divisie}</p>
        <p>Afdeling: ${afdeling}</p>
        <p>Team: ${team}</p>
        <h4 class="sub-titel">Project gegevens</h4>
        <div class="email-info">
            <p>Projectnaam: ${projectnaam}</p>
            <p>Projectlead naam: ${pjnaam}</p>
            <p>Projectlead email: ${pjemail}</p>
            <p>Project type: ${ptype}</p>
            </div>
            <h4 class="sub-titel teamlid-info">Teamleden info</h4>                   
            <table width="50%">
                <tr>
                    <td>
                        <table>
                            <tr><th>Naam: </tr></th>
                            ${teamlidnaam}
                        </table>
                    </td>
                    <td>
                        <table>
                            <tr style="text-align:left;"><th>Email: </tr></th>
                            ${teamlidemail}
                        </table>
                    </td>
                    <td>
                        <table>
                            <tr><th>Adminrechten: </tr></th>
                            ${teamlidrechten}
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        </body>
        </html>
        `;

        const transporter = nodemailer.createTransport({
            host: 'smtp.ethereal.email',
            port: 587,
            auth: {
                user: 'olx5bvx3q4tfqfhb@ethereal.email',
                pass: 'mycEWqhVWjcrS8BPCC'
            }
        });

        let mailOptions = {
            from: '"Agile portal" <agile@minbzk.com>', // sender address
            to: `${email}`, // list of receivers
            subject: 'Jira project aanvraag', // Subject line
            text, // plain text body
            html // html body
        };

        transporter.sendMail(mailOptions, (error, info) => {
            console.log('Message sent: %s', info.messageId);
            // Preview only available when sending through an Ethereal account
            console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
        });

    })
})

//verstuur email functie ~ niks aanpassen graag
app.post('/api/form', (req, res) => {
    nodemailer.createTestAccount((err, account) => {
        const id = req.body.id;
        const naam = req.body.naam;
        const email = req.body.email;
        const divisie = req.body.divisie;
        const afdeling = req.body.afdeling;
        const team = req.body.team;
        const projectnaam = req.body.projectnaam;
        const pjnaam = req.body.pjnaam;
        const pjemail = req.body.pjemail;
        const tlnaam = req.body.tlnaam;
        const tlemail = req.body.tlemail;
        const ptype = req.body.ptype;
        const teamledenemailTable = req.body.teamledenemail.map((i) => {
            return `
                        <tr><td>${i}</td></tr>         
                    `
        }).join(" ");
        const teamledennaamTable = req.body.teamledennaam.map((i) => {
            return `
                       <tr><td>${i}</td></tr>
                       
                    `
        }).join(" ");
        const teamledenrechtenTabel = req.body.teamledenrechten.map((i) => {
            return `
                         <tr><td>${i}</td></tr>  
                    `
        }).join(" ");
        const copy = 
        `
                <html>
                <head>
                    <style>
                        .titel{
                            font-size:25px;
                            color:#39870c;
                        }
                        .sub-titel{
                            color:#39870c;
                            font-weight:400;
                        }
                        table {
                            font-family: arial, sans-serif;
                            border-collapse: collapse;
                            width: 100%;
                        }
                        td, th {
                            border: 1px solid #39870c;
                            text-align: left;
                            padding: 8px;
                        }
                        tr:nth-child(even) {
                            background-color: #6cb842;
                        }
                        .teamlid-info{
                            margin-bottom: 0px;
                        }
                    </style>
                </head>
                <body>
                    <h3 class="titel">Beste ${naam},</h3>
                    <p>Bedankt voor het aanvragen van een Jira project. Hij wordt zo snel mogelijk in behandeling genomen.<br/>
                    Voor belangrijke vragen kun je een email sturen naar agile@minbzk.nl</p>
                    <p>Hieronder de ingevulde data:</p>
                    <h4 class="sub-titel">Persoonlijke gegevens</h4>
                    <div class="email-info">
                        <p>Naam: ${naam}</p>
                        <p>Email: ${email}</p>
                        <p>Divisie: ${divisie}</p>
                        <p>Afdeling: ${afdeling}</p>
                        <p>Team: ${team}</p>
                    </div>
                    <h4 class="sub-titel">Project gegevens</h4>
                    <div class="email-info">
                        <p>Projectnaam: ${projectnaam}</p>
                        <p>Projectlead naam: ${pjnaam}</p>
                        <p>Projectlead email: ${pjemail}</p>
                        <p>Project type: ${ptype}</p>
                    </div>
                    <h4 class="sub-titel teamlid-info">Teamleden info</h4>                   
                    <table width="50%">
                        <tr>
                            <td>
                                <table>
                                    <tr><th>Naam: </tr></th>
                                    ${teamledenemailTable}
                                </table>
                            </td>
                            <td>
                                <table>
                                    <tr style="text-align:left;"><th>Email: </tr></th>
                                    ${teamledennaamTable}
                                </table>
                            </td>
                            <td>
                                <table>
                                    <tr><th>Adminrechten: </tr></th>
                                    ${teamledenrechtenTabel}
                                </table>
                            </td>
                        </tr>
                    </table>
                </body>
            </html>
        `;
        const html = 
        `
                <html>
                <head></head>
                <body>
                    <h3 class="test">Stap een</h3>
                    <p>AanvraagID: ${id}</p>
                    <p>Naam: ${naam}</p>
                    <p>Email: ${email}</p>
                    <p>Divisie: ${divisie}</p>
                    <p>Afdeling: ${afdeling}</p>
                    <p>Team: ${team}</p>
                    <h3 class="titel">Stap 2</h3>
                    <p>Projectnaam: ${projectnaam}</p>
                    <p>Projectlead naam: ${pjnaam}</p>
                    <p>Projectlead email: ${pjemail}</p>
                    <p>Project type: ${ptype}</p>
                    <h3 class="titel">Stap 3</h3>
                    <table width="50%">
                        <tr>
                            <td>
                                <table>
                                    <tr><th>Naam: </tr></th>
                                    ${teamledenemailTable}
                                </table>
                            </td>
                            <td>
                                <table>
                                    <tr style="text-align:left;"><th>Email: </tr></th>
                                    ${teamledennaamTable}
                                </table>
                            </td>
                            <td>
                                <table>
                                    <tr><th>Adminrechten: </tr></th>
                                    ${teamledenrechtenTabel}
                                </table>
                            </td>
                        </tr>
                    </table>
                </body>
            </html>
        `;

      
        const text = 
        `
            Project aanvraag
            naam: ${naam}
            Email: ${email}
            Afdeling: ${afdeling}
            Team: ${team}
            Projectnaam: ${projectnaam}
            Projectlead naam: ${pjnaam}
            Projectlead email: ${pjemail}
            Teamlid naam: ${tlnaam}
            Teamlid email: ${tlemail}
            Project type: ${ptype}
        `;
        // create reusable transporter object using the default SMTP transport
        const transporter = nodemailer.createTransport({
            host: 'smtp.ethereal.email',
            port: 587,
            auth: {
                user: 'olx5bvx3q4tfqfhb@ethereal.email',
                pass: 'mycEWqhVWjcrS8BPCC'
            }
        });
        
        const transporterCopy = nodemailer.createTransport({
            host: 'smtp.ethereal.email',
            port: 587,
            auth: {
                user: 'olx5bvx3q4tfqfhb@ethereal.email',
                pass: 'mycEWqhVWjcrS8BPCC'
            }
        });
    
        // setup email data with unicode symbols
        let mailOptions = {
            from: '"djamaile" <rdjamaile@gmail.com>', // sender address
            to: 'agile@minbzk.nl', // list of receivers
            subject: 'Jira project aanvraag', // Subject line
            text: text, // plain text body
            html: html // html body
        };

        let mailOptionsCopy = {
            from: '"Agile portal" <agile@minbzk.nl>', // sender address
            to: `${email}`, // list of receivers
            subject: 'Jira project aanvraag', // Subject line
            text: text, // plain text body
            html: copy // html body
        };
    
        // send mail with defined transport object
       transporter.sendMail(mailOptions, (error, info) => {
            console.log('Message sent: %s', info.messageId);
            // Preview only available when sending through an Ethereal account
            console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
        });

        transporterCopy.sendMail(mailOptionsCopy, (error, info) => {
            console.log('Message sent: %s', info.messageId);
            // Preview only available when sending through an Ethereal account
            console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
        });
    });
});

app.use(cors({origin:true,credentials: true}));

app.get('*', (req, res) => {
    res.sendFile(path.join(publicpath, 'index.html'));
});

app.listen(port, () => {
    console.log('Server is up!');
});
