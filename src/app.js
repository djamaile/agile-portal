import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import AppRouter from './routers/AppRouter';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import ApolloClient from "apollo-boost";
import {ApolloProvider} from 'react-apollo';
import 'normalize.css/normalize.css';
import './styles/styles.scss';
import { CookiesProvider } from 'react-cookie';


export const MyContext = React.createContext();

// Then create a provider Component
export class MyProvider extends Component {
  state = {
    name: 'onbekend'
  }
  render() {
    return (
      <MyContext.Provider value={{
        state: this.state,
        setUsername: (naam) => this.setState({
          name: naam
        })
      }}>
        {this.props.children}
      </MyContext.Provider>
    )
  }
}
const theme = createMuiTheme({
    palette: {
      primary: {
        main: '#39870c',
        ligth: '#6cb842',
        dark: '#005900'
      },
      secondary: {
        light: '#01689b',
        main: '#0044ff',
        contrastText: '#ffcc00',
      },
    },
    typography: {
      useNextVariants: true,
    },
    overrides: {
      MuiInputLabel: { 
        root: { 
          color:'black',
          fontSize: 13, 
        },
      },
      MuiSelect:{
        selectMenu:{
          fontSize:13
        }
      },
      MuiSvgIcon:{
        root:{
          cursor: 'pointer'
        }
      },
      MuiFormHelperText:{
        root:{
          fontSize:12,
          color:'red'
        }
      }
    }
  });  

  

  const client = new ApolloClient({
    uri: "/graphql"
  });

  const jsx = (
    <ApolloProvider client={client}>
          <MuiThemeProvider theme={theme}>
            <CookiesProvider>
              <MyProvider>
                <AppRouter/>
              </MyProvider>
            </CookiesProvider>
          </MuiThemeProvider>
    </ApolloProvider>
  );

  ReactDOM.render(jsx, document.getElementById('app'));