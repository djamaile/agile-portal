/**
 * Agile portal, Een webportal ontwikkeld door IT4IT&Testmanagement
 * Copyright (C) 2018, SSC-ICT
 * mailto: agile@minbzk.nl
 */

 //imports
import React from "react";
import StapEen from "./stappen/AppStapEen";
import StapTwee from "./stappen/AppStapTwee";
import Teamleden from "./stappen/AppTeamleden";
import StapDrie from "./stappen/AppStapDrie";
import BedanktPagina from "./stappen/AppBedankt";
import axios from "axios";
import { withStyles } from "@material-ui/core/styles";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import Button from "@material-ui/core/Button";
import EmailDomeinen from "../data/emailarray";

//styles van de formulier
const styles = theme => ({
  root: {
    width: "90%",
  },
  stepper: {
    backgroundColor: "white",
  },
  container: {
    height: 240,
    [theme.breakpoints.up('lg')]: {
      width: 1280,
    },
  },
  backButton: {
    marginRight: theme.spacing.unit
  },
  nextButton: {},
  instructions: {
    marginTop: theme.spacing.unit,
    marginBottom: theme.spacing.unit
  },
  projectTitel: {
    fontSize: 32,
    color: "#005900",
    marginLeft: 25
  },
  form: {
    display: "flex",
    alignItems: "baseline",
    justifyContent: "space-evenly"
  },
  error: {
    color: "red",
    textAlign: "center"
  }
});

const teamlidInfo = {
  fontSize: 13,
};

//stappen van de stepper
const Stappen = () => {
  return [
    <p style={teamlidInfo}>Persoonlijke informatie</p>,
    <p style={teamlidInfo}>Project gegevens</p>,
    <p style={teamlidInfo}>Teamleden</p>,
    <p style={teamlidInfo}>Confirmatie</p>,
  ];
};

class AanvraagFormulier extends React.Component {
  constructor(props) {
    super(props);
    //states van alle props
    this.state = {
      activeStep: 0,
      //wordt random 16 id length gegenereerd, met slice reduce ik het tot 6
      id: Math.floor(100000 + Math.random() * 900000),
      projectid: Math.floor(100000 + Math.random() * 900000),
      //alle states van de formulier velden
      naam: '',
      email: '',
      afdeling:  '',
      team:'',
      divisie:  '',
      aanvragerIsProjectLead: 'Ja',
      SelectProjectLeadValue:'',
      //stap twee
      projectnaam:  '',
      pjnaam: '',
      pjemail: '',
      ptype: '',
      //states om de errors bij te houden, bij true komt er een error class bij
      error: '',
      naamError: false,
      emailError: false,
      afdelingError: false,
      teamError: false,
      pjnaamError: false,
      pjleadError: false,
      pjmailError: false,
      tlnaamError: false,
      tlemailError: false,
      tlrowError: false,
      divisieError: false,
      projecttypeError: false,
      errorTeamlid: false,
      projectLeadKeuzeError: false,
      //state van de teamleden
      rows: [],
      rowIndex: 0
    };
  }

  componentDidMount() {
    this.addRow();
  }

  handleStapEen = () => {
    const naamError = this.handelError(this.state.naam, "naamError");
    const emailError = this.handelEmailError(this.state.email, "emailError");
    const afdelingError = this.handelError(this.state.afdeling, "afdelingError");
    const divisieError = this.handelError(this.state.divisie, "divisieError");
    const teamError = this.handelError(this.state.team, "teamError");
    const projectLeadKeuzeError = this.handelError(this.state.aanvragerIsProjectLead,"projectLeadKeuzeError");
    const aanvragerIsProjectLead = this.aanvragerGegevensOverZetten(this.state.aanvragerIsProjectLead);
    
    if (!naamError || !emailError || !afdelingError || !divisieError || !teamError || !projectLeadKeuzeError){
      return true;
    }else{
      return false;
    }

  }

  handleStapTwee = () => {
      //check error validation van stap twee, dit wordt alleen uitgevoerd als de gebruiker bij stap 2 is.
      const pjnaamError = this.handelError(this.state.projectnaam, "pjnaamError");
      const pjleadError = this.handelError(this.state.pjnaam, "pjleadError");
      const pjmailError = this.handelEmailError(this.state.pjemail,"pjmailError");
      const projecttypeError = this.handelError(this.state.ptype,"projecttypeError");

      if (!pjnaamError || !pjleadError || !pjmailError || !projecttypeError) {
          return true;
      } else {
          return false;
      } 
  }

  handleStapDrie = async () => {
    await this.setState(previousState => {
      const team = [...previousState.rows];
      const validDomains = EmailDomeinen(); //pak de valid domains op
      for (let i = 0; i < team.length; i++) {
        if (!team[i].name) {
          team[i].errorNaam = true;
        } else {
          team[i].errorNaam = false;
        }
        const userDomain = team[i].email.split("@")[1];
        if (!validDomains.some(domein => domein === userDomain)) {
          team[i].errorEmail = true;
        } else {
          team[i].errorEmail = false;
        }
      }
    });
    //tenminst een teamlid toevoegen
    const isValid = this.state.rows.length === 0;
    const isTrue = value => value === true || value === undefined;
    const errorNaam = this.state.rows.map(x => x.errorNaam);
    //pak alle erroremails
    const errorEmail = this.state.rows.map(x => x.errorEmail);
    //als een van de errors true of undefined is mag de gebruiker niet naar de volgende stap
    if (errorNaam.some(isTrue) || errorEmail.some(isTrue) || isValid){
      return true;
    }else{
      return false;
    }
  }

  /*
        handleNext wordt getriggerd als de gebruiker naar de volgende stap wilt gaan.
        Als eerst pakken we de boolean waardes van de error validation functies. 
        Als een van hun een false waarde terug stuurt kan de gebruiker niet verder.
        Als alles goed is dan krijgt state.activeStep + 1. 
    */
   handleNext = async () => {
    const stapEen = this.handleStapEen();
    if(this.state.activeStep === 1){
        const stapTwee = this.handleStapTwee();
        if (stapTwee === false) {
            this.setState(state => ({
                  activeStep: state.activeStep + 1
            }));
          } 
    }else if(this.state.activeStep === 2){
            const stapDrie = await this.handleStapDrie();
            if (stapDrie === false){
              this.setState(state => ({
                    activeStep: state.activeStep + 1
              }));
            }
    }else{
        if (stapEen === false) {
          this.setState(state => ({
                   activeStep: state.activeStep + 1
          }));      
        }      
    }
  }

  //een stap terug
  handleBack = () => {
    this.setState(state => ({
      activeStep: state.activeStep - 1
    }));
  };

  aanvragerGegevensOverZetten = (isProjectLead) =>{
    if(isProjectLead === 'Ja'){
      this.setState(state => ({
        pjnaam: state.naam, 
        pjemail: state.email
      }));
    }
  }

  /**
   * Hiermee wordt er een nieuwe rij gestopt in state.rows
   * tlEmailError doet op dit moment nog niks.
   * Ik heb het erin gelaten, zodat het misschien gebruikt kan worden om specifiek aantegeven
   * welk rij een fout waarde bevat.
   */
  addRow = () => {
    this.setState(previousState => {
      return {
        rows: [
          ...previousState.rows,
          {
            name: "",
            email: "",
            tlAdmin: false,
            errorNaam: undefined,
            errorEmail: undefined
          }
        ]
      };
    });
  };

  incrementIndex = () => {
    this.setState(state => ({
      rowIndex: state.rowIndex + 1
    }));
  }

  decrementIndex = () => {
    this.setState(state => ({
      rowIndex: state.rowIndex - 1
    }))
  }

  //Rij van state.rows wordt hiermee verwijderd
  removeRow = index => {
    this.setState(previousState => {
      const rows = [...previousState.rows];
      rows.splice(index, 1);
      return { rows };
    });
  };

  //onChange functie van de teamleden rijen
  onChange = (event, index) => {
    const { name, value } = event.target;

    this.setState(previousState => {
      const rows = [...previousState.rows];
      rows[index] = { ...rows[index], [name]: value };
      return { rows };
    });
  };

  //onchange functie voor admin check
  onSelectChange = (event, index) => {
    const { name } = event.target;
    const value = event.target.checked;

    this.setState(previousState => {
      const rows = [...previousState.rows];
      rows[index] = { ...rows[index], [name]: value };
      return { rows };
    });
  };

  /**
   * Error validatie voor elke tekstveld, statError is voor styling
   */
  handelError = (state, stateError) => {
    if (!state) {
      this.setState({ [stateError]: true });
      return false;
    } else {
      this.setState(() => ({ [stateError]: false }));
      return true;
    }
  };

  handelEmailError = (state, stateError) => {
    var array = EmailDomeinen(); //valide emails
    if (!state) {
      //als email leeg is
      this.setState(() => ({ [stateError]: true })); //error krijgt styling
      return false;
    } else {
      for (let i = 0; i < array.length; i++) {
        let email = array[i];
        //gebruik res om te kijken of de gebruiker wel iets voor de @ domein iets heeft ingevuld.
        const res = state.split("@", 1).toString();

        if (!state.endsWith("@" + email)) {
          //check of ingevuld email eindigt met een van de valide domeinen
          this.setState(() => ({ [stateError]: true }));
        } else {
          if (res) {
            this.setState(() => ({ [stateError]: "" }));
            return true;
          }
        }
      }
      return false;
    }
  };

  //Alle onchange functies
  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  //verzend een email met axios
  onSubmit = e => {
    e.preventDefault(); //zorgt ervoor dat pagina niet opnieuw herlaad

    //gaat naar bedank schermpje
    this.setState(state => ({
      activeStep: state.activeStep + 1
    }));

    //pak de waardes van state
    const {
      id,
      naam,
      email,
      divisie,
      afdeling,
      team,
      projectnaam,
      pjnaam,
      pjemail,
      projectid,
      ptype
    } = this.state;
    const teamledennaam = this.state.rows.map(x => x.name);
    const teamledenemail = this.state.rows.map(x => x.email);
    const teamledenrechten = this.state.rows.map(x => x.tlAdmin.toString() === 'true' ? 'Ja' : 'Nee');
    
    //aanvraag wordt verstuurd naar backend (./server/server.js)
    axios.post("/api/form", {
      id,
      naam,
      email,
      divisie,
      afdeling,
      team,
      projectnaam,
      pjnaam,
      pjemail,
      ptype,
      teamledennaam,
      teamledenemail,
      teamledenrechten
    });

    this.props.onSubmit({
      id,
      projectid,
      naam,
      email,
      divisie,
      afdeling,
      team,
      projectnaam,
      pjnaam,
      pjemail,
      ptype,
      teamledennaam,
      teamledenemail,
      teamledenrechten
    });
  };

  //Alle Stappen classes worden hier gedefinieerd met hun props
  StappenContent = index => {
    switch (index) {
      case 0:
        return (
          <StapEen
            onNaamChange={this.handleChange}
            naamValue={this.state.naam}
            onEmaileChange={this.handleChange}
            emailValue={this.state.email}
            handleAfdelingChange={this.handleChange}
            SelectAfdelingValue={this.state.afdeling}
            handleTeamChange={this.handleChange}
            handleDivisieChange={this.handleChange}
            SelectTeamValue={this.state.team}
            SelectDivisieValue={this.state.divisie}
            naamError={this.state.naamError}
            emailError={this.state.emailError}
            afdelingError={this.state.afdelingError}
            teamError={this.state.teamError}
            divisieError={this.state.divisieError}
            onSelectChange={this.handleChange}
            projectLeadKeuzeError={this.state.projectLeadKeuzeError}
            aanvragerIsProjectLead={this.state.aanvragerIsProjectLead}
            handleProjectLeadKeuzeChange={this.handleChange}
          />
        );
      case 1:
        return (
          <StapTwee
            pjnaamError={this.state.pjnaamError}
            pjleadError={this.state.pjleadError}
            pjleadEmailError={this.state.pjmailError}
            tlnaamError={this.state.tlnaamError}
            tlemailError={this.state.tlemailError}
            projecttypeError={this.state.projecttypeError}
            projectNaamValue={this.state.projectnaam}
            onProjectNaamChange={this.handleChange}
            projectleadNaamValue={this.state.pjnaam}
            onProjectleadNaamChange={this.handleChange}
            projectleadEmailValue={this.state.pjemail}
            onProjectleadEmailChange={this.handleChange}
            projectTypeValue={this.state.ptype}
            onprojectTypeChange={this.handleChange}
          />
        );
      case 2:
        return (
          //teamleden is er laterbijgekomen vandaar dat het geen stap is
          <Teamleden
            rows={this.state.rows}
            removeRow={this.removeRow}
            onChange={this.onChange}
            addRow={this.addRow}
            tlemailError={this.state.tlemailError}
            tlnaamError={this.state.tlnaamError}
            tlrowError={this.state.tlrowError}
            errorTeamlid={this.state.errorTeamlid}
            onSelectChange={this.onSelectChange}
            rowIndex={this.state.rowIndex}  
            incrementIndex={this.incrementIndex}
            decrementIndex={this.decrementIndex}
          />
        );
      case 3:
        return (
          <StapDrie
            naam={this.state.naam}
            email={this.state.email}
            divisie={this.state.divisie}
            afdeling={this.state.afdeling}
            team={this.state.team}
            pjnaam={this.state.projectnaam}
            pjlnaam={this.state.pjnaam}
            pjlemail={this.state.pjemail}
            teamledennaam={this.state.rows.map(x => (
              <li key={x.email} style={teamlidInfo}>{x.name}</li>
            ))}
            teamledenemail={this.state.rows.map(x => (
              <li key={x.email} style={teamlidInfo}>{x.email}</li>
            ))}
            teamledenrechten={this.state.rows.map(x => (
              <li key={x.email} style={teamlidInfo}>{x.tlAdmin === true ? 'Ja' : 'Nee'}</li>
            ))}
            pjtype={this.state.ptype}
          />
        );
    }
  };

  
  gaNaarStapEenViaIcon = () => {
    for(let i = this.state.activeStep; i > 0; i--){
      this.setState(state => ({
            activeStep: state.activeStep - 1
      }));
    }
  }

  gaNaarStapTweeViaIcon = () => {
    if(this.state.activeStep === 0){
      const stapEen = this.handleStapEen();
      if(stapEen === false){
        this.setState(state => ({
            activeStep: state.activeStep + 1
        }));
      }
    }else if(this.state.activeStep >= 1){
      for(let i = this.state.activeStep; i > 1; i--){
        this.setState(state => ({
              activeStep: state.activeStep - 1
        }));
      }
    }
  }

  gaNaarStapDrieViaIcon = () => {
    if(this.state.activeStep === 0){
      const stapEen = this.handleStapEen();
      const stapTwee = this.handleStapTwee();
      if(stapEen === false && stapTwee === false){
        this.setState(state => ({
            activeStep: state.activeStep + 2
        }));
      }
    }else if(this.state.activeStep === 1){
      const stapTwee = this.handleStapTwee();
      if(stapTwee === false){
        this.setState(state => ({
              activeStep: state.activeStep + 1
        }));
      }
    }else if(this.state.activeStep >= 2){
      for(let i = this.state.activeStep; i > 2; i--){
        this.setState(state => ({
              activeStep: state.activeStep - 1
        }));
      }
    }
  }

  gaNaarStapVierViaIcon = async () => {
    if(this.state.activeStep === 0){
      const stapEen = this.handleStapEen();
      const stapTwee = this.handleStapTwee();
      const stapDrie = await this.handleStapDrie();
      if(stapEen === false && stapTwee === false && stapDrie === false){
        this.setState(state => ({
            activeStep: state.activeStep + 3
        }));
      }
    }else if(this.state.activeStep === 1){
      const stapTwee = this.handleStapTwee();
      const stapDrie = await this.handleStapDrie();
      if(stapTwee === false && stapDrie === false){
        this.setState(state => ({
            activeStep: state.activeStep + 2
        }));
      }
    }else if(this.state.activeStep === 2){
      const stapDrie = await this.handleStapDrie();
      if(stapDrie === false){
        this.setState(state => ({
              activeStep: state.activeStep + 1
        }));
      }
    }else if(this.state.activeStep >=3){
      for(let i = this.state.activeStep; i > 3; i--){
        this.setState(state => ({
              activeStep: state.activeStep - 1
        }));
      }
    }
  }

  gaNaarStapViaIcon = (label) => {
    //stap een
    if(label === 'Persoonlijke informatie'){
      this.gaNaarStapEenViaIcon();
    }
    //stap twee
    else if(label === 'Project gegevens'){
      this.gaNaarStapTweeViaIcon();
    }
    //stap drie
    else if(label === 'Teamleden'){
      this.gaNaarStapDrieViaIcon();
    }
    //stap vier
    else if(label === 'Confirmatie'){
      this.gaNaarStapVierViaIcon();
    }
  }

  //html code plus stappen worden hier gerenderd
  render() {
    const { classes } = this.props;
    const steps = Stappen();
    const { activeStep } = this.state;

    return (
      <div className="container">
        <div className="col s12">
          <h2 className={classes.projectTitel}>Project aanvragen</h2>
          <Stepper
            activeStep={activeStep}
            alternativeLabel
            className={classes.stepper}
          >
            {steps.map(label => {
              return (
                <Step key={label}>
                  <StepLabel onClick={() => this.gaNaarStapViaIcon(label.props.children)}>{label}</StepLabel>
                </Step>
              );
            })}
          </Stepper>
          <div>
            {this.state.activeStep === steps.length ? (
              <div>
                <BedanktPagina generateID={this.state.id} />
              </div>
            ) : (
              <div>
                <form onSubmit={this.onSubmit}>
                  {this.StappenContent(activeStep)}
                  <div>
                    {this.state.activeStep > 0 && (
                      <Button
                        disabled={activeStep === 0}
                        onClick={this.handleBack}
                        className={classes.backButton}
                      >
                        Terug
                      </Button>
                    )}

                    {this.state.activeStep === steps.length - 1 ? (
                      <Button
                        variant="contained"
                        color="primary"
                        onClick={this.onSubmit}
                      >
                        Versturen
                      </Button>
                    ) : (
                      <Button
                        variant="contained"
                        color="primary"
                        onClick={this.handleNext}
                      >
                        Volgende
                      </Button>
                    )}
                  </div>
                </form>
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(AanvraagFormulier);
