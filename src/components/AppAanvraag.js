/**
 * Agile portal, Een webportal ontwikkeld door IT4IT&Testmanagement
 * Copyright (C) 2018, SSC-ICT
 * mailto: agile@minbzk.nl
 */

 //imports
import React, {Suspense} from 'react';
import gql from 'graphql-tag';
import {graphql,compose, Mutation} from 'react-apollo';
import AanvraagFromulier from './AanvraagFormulier';
import moment from 'moment';

export class Aanvraag extends React.Component{
    /**
     * Hier wordt aanvraag verstuurd naar de backend en opgeslagen in de database.
     * Dit wordt gedaan met behulp van GraphQL. Functie kan gefactoreerd worden? 
     * Functie werkt op: 7-11-2018
     */
    onSubmit = (aanvraag) => {
        //length van teamleden array bijhouden
        var length = aanvraag.teamledennaam.length;
        //zet teamleden waardes in veschillende arrays
        var teamledennamen = aanvraag.teamledennaam;
        var teamledenemails = aanvraag.teamledenemail;
        var adminrechten = aanvraag.teamledenrechten;
        //datum is Date.now(), maar dan geformatteerd met behulp van moment 
        var datum = moment().format('D MMM, YYYY');

        //Roep addAanvraag op
        this.props.aanvraagmutate({
            variables: {
                id: aanvraag.id,
                naam: aanvraag.naam,
                email: aanvraag.email.toLowerCase(),
                afdeling: aanvraag.afdeling,
                divisie: aanvraag.divisie,
                team: aanvraag.team,
                status: 'open',
                datum
            },
        });
        //Roep addProject op
        this.props.projectmutate({
            variables: {
                //project vars
                projectid: aanvraag.projectid,
                projectnaam: aanvraag.projectnaam,
                projecttype: aanvraag.ptype,
                projectlead_naam: aanvraag.pjnaam,
                projectlead_email:aanvraag.pjemail.toLowerCase(),
                aanvraag_id:aanvraag.id
            },
        });
        /**
         * Kijk eerst hoeveel teamleden e rzijn. 
         * teamlid wordt toegvoegd keer het aantal teamleden
         * Dit wordt gedaan met een for loop, waarschijnlijk is er een betere manier. 
         * O notatie = O(n) == ??
         */
        for(let i = 0; i < length; i++){
            var naam = teamledennamen[i];
            var email = teamledenemails[i];
            var rechten = adminrechten[i];

            this.props.teamlidmutate({
                variables: {
                    id: Math.floor(100000 + Math.random() * 900000),
                    naam: naam,
                    email: email.toLowerCase(),
                    aanvraag_id: aanvraag.id,
                    project_id: aanvraag.projectid,
                    adminrechten: rechten
                }
            });
        }
    }
    
    render(){
        return(
            <div>
               <AanvraagFromulier
                    onSubmit={this.onSubmit}
                />
            </div>
        );
    }
}

///GraphQL mutation, kan in een eigen file zetten. 
const aanvraagmutation = gql`
    mutation addAanvraag($id:ID, $naam:String,$email:String,$divisie:String,$afdeling:String,$team:String,$status:String,$datum:String){
    addAanvraag(id:$id, naam:$naam,email:$email,divisie:$divisie,afdeling:$afdeling,team:$team,status:$status,datum:$datum){
      id
     }
    }
`;

///GraphQL mutation, kan in een eigen file zetten. 
const projectmutation = gql`
mutation addProject($projectid:ID,$projectnaam:String,$projecttype:String,$projectlead_naam:String,$projectlead_email:String,$aanvraag_id:ID){
    addProject(id:$projectid, naam:$projectnaam,type:$projecttype,lead_naam:$projectlead_naam,lead_email:$projectlead_email,aanvraag_id:$aanvraag_id){
      id
    }
  }  
`;

///GraphQL mutation, kan in een eigen file zetten. 
const teamlidmutation = gql`
mutation addTeamlid($id:ID,$naam:String,$email:String, $aanvraag_id:ID, $project_id:ID,$adminrechten:String){
    addTeamlid(id:$id, naam:$naam,email:$email,aanvraag_id:$aanvraag_id,project_id:$project_id,adminrechten:$adminrechten){
      id
    }
  }  
`;

/**
 * Met compose kan je meerdere mutations meegeven aan class, 
 * zorg er wel voor dat je een name meegeeft.
 * 
 * export
 */
export default  compose(
    graphql(teamlidmutation, { name: 'teamlidmutate' }),
    graphql(projectmutation, {name: 'projectmutate'}),
    graphql(aanvraagmutation, { name: 'aanvraagmutate' }),
 )(Aanvraag);