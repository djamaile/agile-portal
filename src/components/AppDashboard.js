/**
 * Agile portal, Een webportal ontwikkeld door IT4IT&Testmanagement
 * Copyright (C) 2018, SSC-ICT
 * mailto: agile@minbzk.nl
 */

 //imports
import React from 'react';
import ComponentKeuze from './ci-cd-straat-products/AppKeuze';

//Index page 
const Dashboard = () => {
   return(
        <React.Fragment>
           <ComponentKeuze/>
        </React.Fragment>
   );
}

//export
export default Dashboard;