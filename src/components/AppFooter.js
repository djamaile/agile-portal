/**
 * Agile portal, Een webportal ontwikkeld door IT4IT&Testmanagement
 * Copyright (C) 2018, SSC-ICT
 * mailto: agile@minbzk.nl
 */

//imports
import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Call from '@material-ui/icons/Call';
import Mail from '@material-ui/icons/Mail';

//CSS styles
const styles = theme => ({
    root: {
      flexGrow: 1,
      marginTop:25,
      backgroundColor: '#39870c',
      height:180,
      flex: '0 0 auto'
    },
    footer:{
        display: 'flex',
        flexDirection: 'row',
        backgroundColor: '#39870c',
    },
    footerMaterial: {
        margin: 15
    },
    footerInfo:{
        paddingLeft: 0,
        color: 'white',
        fontSize: 21,
        margin: 17.44,
        marginLeft: 0,
        fontWeight: 400
    }, 
    footerUl:{
        paddingLeft: 0,
        listStyleType: 'none'
    }, 
    footerLi:{
        fontSize:16,
        lineHeight: 1.5,
        listStyleType: 'none'
    },
    footerLink:{
        color: '#eee',
        textDecoration: 'none',
        '&:hover': {
            color: 'black'
          }
    },
    footerNummer:{
        color: '#eee',
        textDecoration: 'none',
    },
    footerTekst:{
        fontStyle: 'italic',
        color: '#eee',
        fontSize: 18
    },
    footerBlok:{
        margin:17.440,
        marginLeft:65
    },
    icon:{
        fontSize: 18, 
        verticalAlign: 'middle',
        marginRight:5
    },
    marginText:{
        marginRight:15,
        marginLeft: 15,
        color:'white'
    },
    marginTextSmall:{
        marginRight:5,
        marginLeft: 5,
        color:'white'
    },
    item:{
        marginLeft: 30
    },
    footerSubTekst:{
        fontSize:12
    }
  });

/**
 * Standaard footer die gebruikt wordt op elke pagina
 * @param {*} props 
 */
const Footer = (props) => {
    const { classes } = props;
    return (
    <div className="footer">
            <Grid container spacing={16}>
                <Grid item xs={12} sm={12} md={5} lg={5} className={classes.item}>
                    <h5 className={classes.footerInfo}>SSC-ICT</h5>
                    <li className={classes.footerLi}>
                        <span><a href="mailto:agile@minbzk.nl" className={classes.footerLink} target="_blank"><Mail className={classes.icon}/>agile@minbzk.nl</a><span className={classes.marginText}>|</span><a className={classes.footerNummer}><Call className={classes.icon}/>0883710354</a></span>
                    </li>
                    <p className={classes.footerTekst}>Ministerie van Binnenlandse Zaken en Koninkrijksrelaties<br/>
                        <span className={classes.footerSubTekst}>Agile portal (Version 0.1)</span></p>
                </Grid>
                <Grid item xs={12} sm={12} md={2} lg={2}>

                </Grid>
                <Grid item xs={12} sm={12} md={4} lg={4}>
                    <div className={classes.footerBlok}>
                    <span>
                        <a  className={classes.footerLink} href="https://jira.rijksweb.nl" target="_blank">Jira</a>
                        <span className={classes.marginTextSmall}>|</span>
                        <a  className={classes.footerLink} href="https://confluence.rijksweb.nl" target="_blank">Confluence</a>
                        <span className={classes.marginTextSmall}>|</span>
                        <a  className={classes.footerLink} href="https://git.rijksweb.nl" target="_blank">Bitbucket</a>
                        <span className={classes.marginTextSmall}>|</span>
                        <a  className={classes.footerLink} href="https://sonarqube.rijksweb.nl" target="_blank">Sonarqube</a>
                        <span className={classes.marginTextSmall}>|</span>
                        <a  className={classes.footerLink} href="" target="_blank">Sharepoint</a>
                    </span>
                    </div>
                </Grid>
            </Grid>
        </div>
    );
};

Footer.propTypes = {
    classes: PropTypes.object.isRequired,
};

//export
export default withStyles(styles)(Footer);