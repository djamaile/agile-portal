/**
 * Agile portal, Een webportal ontwikkeld door IT4IT&Testmanagement
 * Copyright (C) 2018, SSC-ICT
 * mailto: agile@minbzk.nl
 */

 //imports
import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Logo from '../../public/images/logo-ssc.png';
import {Link} from 'react-router-dom';

//CSS style
const styles = theme =>({
    root: {
      flexGrow: 1,
    },
    grow: {
      flexGrow: 1,
    },
    menuButton: {
      marginLeft: -12,
      marginRight: 20,
    },
    toolbar:{
        left: '47%',
        [theme.breakpoints.up('md')]: {
            left: '47%',
        },
        [theme.breakpoints.up('lg')]: {
            left: '47%',
        },
        [theme.breakpoints.down('sm')]: {
            left: '0%',
        },
    }
  });

  /**
   * Header wordt op elke pagina gebruikt. Logo is ook hier te vinden
   * @param {*} props 
   */
const Header = (props) => {
    const { classes } = props;
    return (
        <div className={classes.root} id="header">
            <AppBar position="static" className="appbar-header">
                <Toolbar className={classes.toolbar}>
                    <Link to="/"><img className="img-logo" src={Logo} alt="logo"/></Link>
                </Toolbar>
            </AppBar>
        </div>
    );
};

Header.propTypes = {
    classes: PropTypes.object.isRequired,
};

//export
export default withStyles(styles)(Header);