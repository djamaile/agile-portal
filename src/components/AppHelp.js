/**
 * Agile portal, Een webportal ontwikkeld door IT4IT&Testmanagement
 * Copyright (C) 2018, SSC-ICT
 * mailto: agile@minbzk.nl
 */

 //imports
import React from 'react';

/**
 * Guide pagina
 */
const Help = () => (
    <div>
        <p>Hier komt een guide</p>
    </div>
);

//export
export default Help;