/**
 * Agile portal, Een webportal ontwikkeld door IT4IT&Testmanagement
 * Copyright (C) 2018, SSC-ICT
 * mailto: agile@minbzk.nl
 */

 //imports
import React from 'react';
import {Link} from 'react-router-dom';

/**
 * Als een gebruiker naar een pagina navigeert die niet bestaat
 * dan komt deze pagina tevoorschijn. 
 */
const NotFound = () => {
    return(
            <div>
                <p>Sorry deze pagina bestaat niet.</p>
                <Link to="/">Home pagina</Link>
            </div>
            
    );
}

//export
export default NotFound;