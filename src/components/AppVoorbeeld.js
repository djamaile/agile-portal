import React from 'react';

class StageVoorbeeld extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            dag: '',
            keuze: '',
            uur:''
        };
    }

    recursion = (number) =>{
        if(number === 1){
            return;
        }
        number--;
        return number + this.recursion(number);
    }

    WatMoetIkDoen = (uur) => {
        this.setState({uur});
        const uurState = this.state.uur;

        if(uurState >= 18 && uurState <= 5){
            this.setState({dag: 'laat'});
            this.setState({keuze: 'slapen'});
        }else{
            this.setState({dag: 'vroeg'});
            this.setState({keuze: 'gamen'});
        }
        return <Plan vroegOflaat={this.state.dag} keuze={this.state.keuze}/>
    }

    render(){
        return(
            <div>
                {this.WatMoetIkDoen(8)}
            </div>
        );
    }
}

const Plan = (props) => {
    return(
        <div>
            <h1>Het is best {props.vroegOflaat}</h1>
            <p>dus wil ik gaan {props.keuze}</p>
        </div>
    );
}

