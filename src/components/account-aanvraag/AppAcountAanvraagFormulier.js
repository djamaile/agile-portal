import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import StapEen from './stappen/stapEen';
import StapTwee from './stappen/stapTwee';
import StapDrie from './stappen/stapDrie';
import Typography from '@material-ui/core/Typography';



const styles = theme => ({
    root: {
      width: '90%',
    },
    backButton: {
      marginRight: theme.spacing.unit,
    },
    instructions: {
      marginTop: theme.spacing.unit,
      marginBottom: theme.spacing.unit,
    },
    projectTitel: {
        fontSize: 32,
        color: "#005900",
        marginLeft: 25
    },
});


const teamlidInfo = {
    fontSize: 13,
};

const Stappen = () => {
    return [
      <p style={teamlidInfo}>Zoek project</p>,
      <p style={teamlidInfo}>Teamleden toevoegen</p>,
      <p style={teamlidInfo}>Bevestigen</p>,
    ];
};


class Formulier extends Component{
    constructor(props){
        super(props);
        this.state={
            activeStep: 0,
            projectNaam: ''
        }
    }

    StappenContent = index => {
        switch (index) {
            case 0:
              return <StapEen
                        projectNaam={this.state.projectNaam}
                        handleChange={this.handleChange}
                    />;
            case 1:
              return <StapTwee/>;
            case 2:
              return <StapDrie/>;
            default:
              return 'Uknown stepIndex';
          }
    }
    

    handleNext = () => {
        this.setState(state => ({
          activeStep: state.activeStep + 1,
        }));
      };
    
      handleBack = () => {
        this.setState(state => ({
          activeStep: state.activeStep - 1,
        }));
      };
    
      handleReset = () => {
        this.setState({
          activeStep: 0,
        });
      };

      handleChange = event => {
        this.setState({ [event.target.name]: event.target.value });
      };
    

      
    render(){
        const { classes } = this.props;
        const steps = Stappen();
        const { activeStep } = this.state;
        console.log(activeStep);
        return (
            <div className="container">
              <div className="col s12">
                <h2 className={classes.projectTitel}>Account(s) aanvragen</h2>
                <Stepper
                  activeStep={activeStep}
                  alternativeLabel
                  className={classes.stepper}
                >
                  {steps.map(label => {
                    return (
                      <Step key={label}>
                        <StepLabel onClick={() => this.gaNaarStapViaIcon(label.props.children)}>{label}</StepLabel>
                      </Step>
                    );
                  })}
                </Stepper>
                <div>
                  {this.state.activeStep === steps.length ? (
                    <div>
                      <BedanktPagina generateID={this.state.id} />
                    </div>
                  ) : (
                    <div>
                      <form onSubmit={this.onSubmit}>
                        {this.StappenContent(activeStep)}
                        <div>
                          {this.state.activeStep > 0 && (
                            <Button
                              disabled={activeStep === 0}
                              onClick={this.handleBack}
                              className={classes.backButton}
                            >
                              Terug
                            </Button>
                          )}
      
                          {this.state.activeStep === steps.length - 1 ? (
                            <Button
                              variant="contained"
                              color="primary"
                              onClick={this.onSubmit}
                            >
                              Versturen
                            </Button>
                          ) : (
                            <Button
                              variant="contained"
                              color="primary"
                              onClick={this.handleNext}
                            >
                              Volgende
                            </Button>
                          )}
                        </div>
                      </form>
                    </div>
                  )}
                </div>
              </div>
            </div>
          );
    }
}

export default withStyles(styles)(Formulier);
