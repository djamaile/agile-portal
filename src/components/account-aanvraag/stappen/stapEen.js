import React, {Component, Fragment} from 'react';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import Details from '@material-ui/icons/Details';
import Button from '@material-ui/core/Button';
import axios from 'axios';



const styles = theme => ({
    container: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    row:{
        marginBottom:0
    },
    textField: {
      marginLeft: theme.spacing.unit,
      marginRight: theme.spacing.unit,
      width: '100%',
      paddingBottom: 15,
    },
    dense: {
      marginTop: 19,
    },
    menu: {
      width: 200,
    },
    formControl: {
        margin: theme.spacing.unit,
        minWidth: 120,
        width: '100%',
        fontSize: 15,
        paddingBottom: 15
    },
    selectEmpty: {
        marginTop: theme.spacing.unit * 2,
    },
    resize:{
        fontSize:13
    },
    button:{
        margin: theme.spacing.unit,
        marginRight: 10,
        margin:8,
        marginTop:13
    },
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
        overflowX: 'auto',
    },
    table: {
        minWidth: 700,
    },
});

const getProject = async () => {
    const response = await axios.post('/api/get-project');
    return response;
}

class Stap extends Component{

    constructor(props){
        super(props);
        this.state = {
            showTeamleden: false,
            project: []
        };
    }

    getTeamLeden = async () => {
        await this.setState({showTeamleden:true});
        const project = await getProject();
        this.setState({project});
    }

    render(){
        const {classes} = this.props;
        return(
            <Fragment>
                <div className="row">
                    <div className="col s6">
                        <TextField
                            id="standard-dense"
                            name="email"
                            label="E-mail"
                            value={this.props.emailValue}
                            maxLength="255"
                            onChange={this.props.onEmailChange}
                            className={classNames(classes.textField, classes.dense)}
                            margin="dense"
                            helperText={this.props.emailError === true && this.props.emailValue.length > 0 ? "Enkel een Rijks e-mailadres is toegestaan" : ''}
                            InputProps={{
                                classes: {
                                input: classes.resize,
                                },
                            }}
                            error={
                                this.props.emailError === true ? 
                                true:false
                            }
                        />
                    </div>
                    <div className="col s2">
                        <Button 
                            mini
                            className={classes.button} 
                            variant="fab" 
                            color="primary" 
                            aria-label="Add" 
                            data-toggle="tooltip"
                            onClick={()=>this.getTeamLeden()}
                            className="btn btn-xs btn-primary"
                        >
                            <Details/>
                        </Button>
                    </div>
                </div>
                <div className="row">
                    <div className="col s6">
                        {this.state.showTeamleden === true ? 
                            <Fragment>
                                <FormControl className={classes.formControl}>
                                    <InputLabel shrink htmlFor="age-label-placeholder">
                                    Project
                                    </InputLabel>
                                    <Select
                                        name="projectNaam"
                                        value={this.props.projectNaam}
                                        onChange={this.props.handleChange}
                                        displayEmpty
                                        className={classes.selectEmpty}
                                    >
                                        <MenuItem value="">
                                            <em>None</em>
                                        </MenuItem>
                                        <MenuItem value={10}>Project een</MenuItem>
                                        <MenuItem value={20}>Project twee</MenuItem>
                                        <MenuItem value={30}>Project drie</MenuItem>
                                    </Select>
                                </FormControl>
                                {this.state.project}
                            </Fragment>
                            :
                            <p>Voer een e-mail adres in</p>
                        }
                    </div>      
                </div>
            
            </Fragment>
        )
    }
}

export default withStyles(styles)(Stap);