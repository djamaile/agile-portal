/**
 * Agile portal, Een webportal ontwikkeld door IT4IT&Testmanagement
 * Copyright (C) 2018, SSC-ICT
 * mailto: agile@minbzk.nl
 */

 //imports
 import React, {Component} from 'react';
 import {graphql} from 'react-apollo';
 import gql from 'graphql-tag';
 import FetchDetail from '../../queries/fetchaanvraagdetail';
 import Button from '@material-ui/core/Button';
 import { withStyles } from '@material-ui/core/styles';
 import {Redirect} from 'react-router-dom';
 import axios from 'axios';
 import Modal from '@material-ui/core/Modal';
 import history from '../../data/history';
 import moment from 'moment';
 import { withCookies, Cookies } from 'react-cookie';
 import { instanceOf } from 'prop-types';
 import green from '@material-ui/core/colors/green';
 import CircularProgress from '@material-ui/core/CircularProgress';
 import {MyContext} from '../../app';

 
 /**
  * Functie om te bepalen waar de modal tevoor schijn komt.
  * Voor meer informatie raadpleeg Material-ui.
  */
 function getModalStyle() {
     const top = 41;
     const left = 46;
 
     return {
         top: `${top}%`,
         left: `${left}%`,
         transform: `translate(-${top}%, -${left}%)`,
     };
 }
 
 //CSS styles
 const style = theme => ({
     titel:{
         fontSize:31,
         color:'#6cb842',
         fontWeight:400
     },
     subTitel:{
         fontSize:21,
         color:'#6cb842',
         fontWeight:400
     },
     bold:{
         fontWeight:'bold'
     },
     ul:{
         listStyle:'none',
         paddingLeft:0,
     },
     button: {
         margin: theme.spacing.unit,
       },
     input: {
         display: 'none',
     },
     progress: {
        display:'block',
        margin: 'auto',

    },
     paper: {
         position: 'absolute',
         width: theme.spacing.unit * 50,
         backgroundColor: theme.palette.background.paper,
         boxShadow: theme.shadows[5],
         padding: theme.spacing.unit * 4,
     },
 });
 
 class AanvraagDetail extends Component{
 
     static propTypes = {
         cookies: instanceOf(Cookies).isRequired
     };

 
     constructor(props){
         super(props);
         this.state = {
             naam: '',
             open: false,
             afwijzing: '',
             projectkey: '',
             projectURL: '',
             username:'',
             aanvraagCompleet: undefined            
         }
     }

     //functie om te bepalen of de modal open dicht moet
     handleOpen = () => {
         this.setState({ open: true });
     };
     
     handleClose = () => {
         this.setState({ open: false });
     };
     
     /**
      * Functies om teamleden te renderen. 
      * Dit kan gerefactoreerd worden tot een functie. 
      * Alle drie functies doen namelijk het zelfde. 
      */
     renderTeamlidNaam(){
         const {aanvraag} = this.props.data;
         return aanvraag.project.teamlid.map(({naam}) => {
             return(
                <li key={naam}>{naam}</li>
             );
         });
     }
     
     renderTeamlidEmail(){
         const {aanvraag} = this.props.data;
         return aanvraag.project.teamlid.map(({email}) => {
             return(
                <li key={email}>{email}</li>
             );
         });
     }
 
 
     renderTeamlidRechten(){
         const {aanvraag} = this.props.data;
         return aanvraag.project.teamlid.map(({naam,adminrechten}) => {
             return(
                <li key={naam}>{adminrechten}</li>
             );
         });
     }
 
     //checkt of gebruiker is ingelogd
     isAuthenticated(){
         const { cookies } = this.props;
         const token = cookies.get('token');
         if(token && token.length > 10){
             return true;
         }else{
             return false;
         }
     }
   
     //generic onChange functie
     handleChange = name => event => {
         this.setState({ [name]: event.target.value });
     };
 
     compleetAanvraag(id,status,beheerder){
         const {aanvraag} = this.props.data;
         //nemen reden mee
         const aanvraagInformatie = {
            "afwijzing": this.state.afwijzing,
            "email":aanvraag.email,
            "naam":aanvraag.naam,
            "divisie":aanvraag.divisie,
            "afdeling":aanvraag.afdeling,
            "team":aanvraag.team,
            "projectnaam":aanvraag.project.naam,
            "pjnaam":aanvraag.project.lead_naam,
            "pjemail":aanvraag.project.lead_email,
            "ptype":aanvraag.project.type,
            "teamlid":aanvraag.project.teamlid,
            "url":this.state.projectURL
         };

         //variabeles om een update uit te voeren
         
         const verwerkt_door = beheerder;
         const verwerkt_op = moment().format('D MMM, YYYY').toString();
         this.props.mutate({
             variables: {id, verwerkt_door, verwerkt_op}
         })
         .then(() => this.props.data.refetch())
         .catch((error) => console.log(error));
         //email wordt verstuurd
         if(status === 'weiger'){
            axios.post('/api/weigeren', {aanvraagInformatie});
         }else if(status === 'accepteer'){
            console.log('geaccepteerd');
            axios.post('/api/accepteren', {aanvraagInformatie});
         }else{
             console.log('toch een error');
         }
         //terug naar aanvraag overzicht
         history.push('/admin');
     }
 

     /**
      * Api om een project aan te maken wordt nu aangeroepen. 
      * Er kan nu nog niet gecheckt worden of de functie succesvol of gefaald is. 
      * Alsjeblieft erbij maken. 
      */
     async verwerkAanvraag(beheerder){
         const {aanvraag} = this.props.data;
         await this.setState({aanvraagCompleet: false})
         const teamleden = [{naam: aanvraag.project.lead_naam, email: aanvraag.project.lead_email}, ...aanvraag.project.teamlid];
         const teamledenMetAdminRechten = [{naam: aanvraag.project.lead_naam, email: aanvraag.project.lead_email}, ...aanvraag.project.teamlid.filter(x => x.adminrechten === 'Ja')];
         if(!aanvraag){
             return;
         }

        await Promise.all(teamleden.map(async (user) => {
            const bestaatGebruiker = await this.zoekGebruiker(user.email);
             if(!bestaatGebruiker){
                 const gebruikerIsAangemaakt = await this.maakGebruikerAan(user.email, user.naam);
                 if(gebruikerIsAangemaakt){
                     this.plaatsGebruikerInGroep(user.email, aanvraag.team);
                     this.geefApplicatieAanGebruiker(user.email);
                 }
             }else{
                 this.plaatsGebruikerInGroep(user.email, aanvraag.team);
                 this.geefApplicatieAanGebruiker(user.email);
             }
        }));

         //maakt project aan
         const projectIsAangemaakt = await this.maakProjectAan(aanvraag.project.naam, aanvraag.project.lead_email, aanvraag.project.type);
         //geef developers rol aan de groep
         if(projectIsAangemaakt){
             this.geefRolAanGroep(aanvraag.team, this.state.projectkey);
             teamledenMetAdminRechten.forEach((user) => {
                this.geefAdminRolAanUser(user.email, this.state.projectkey);
             });
         }
         //accepteer functie aanroepen
         this.compleetAanvraag(aanvraag.id,"accepteer", beheerder)
     }
 
     geefApplicatieAanGebruiker(username){
         const user = username;
         axios.post('/api/add-user-to-application', {user});
     }
     
     plaatsGebruikerInGroep(username, group){
         const user = username;
         const groupname = group;
         axios.post('/api/add-to-group', {groupname, user})
             .catch((err) => console.log(err));
     }
 
     async maakGebruikerAan(email, naam){
         const {aanvraag} = this.props.data;
         if(!aanvraag){return;}
         const username = email;
         const displayname = naam;
         const response = await axios.post('/api/new-user', {username, displayname});
         if(response !== 'error'){
             return true;
         }else{
             return false;
         }
     }
 
     async zoekGebruiker(user){
         const gebruikersnaam = user;
         const response = await axios.post('/api/find-user', {gebruikersnaam});
         if(!response.data.err){
             return true;
         }else{
             return false;
         }
     }
 
     async maakProjectAan(naam, lead,type){
         const projectnaam = naam;
         const projectlead = lead;
         const projectType = type;
         const result = await axios.post('/api/new-project',{projectnaam, projectlead,projectType});
         if(result){
             this.setState({projectkey: result.data.data.key});
             this.setState({projectURL: `http://35.185.45.117:8080/projects/${result.data.data.key}/summary`});
             return true;
         }else{
             return false;
         }
     }
 
    geefRolAanGroep(group, key){
         const groupname = group;
         const projectkey = key;
         axios.post('/api/add-role-to-group', {groupname, projectkey});
     }
 
     geefAdminRolAanUser(username, key){
         const user = username;
         const projectkey = key;
         axios.post('/api/add-admin-role-to-person', {user, projectkey});
     }

     hanldeUsername(value){
         this.setState({username:value});
     }
 
     render(){
         const {aanvraag} = this.props.data;
         const{classes} = this.props;
         console.log(this.state.static);
         if(this.props.data.loading){return  (
            <div className="container">
                <div className="row">
                    <div className="col s12">
                        <CircularProgress className={classes.progress} style={{ color: green[500] }} thickness={7} />
                    </div>
                </div>
            </div>
         )}
         if(this.state.aanvraagCompleet === false){
            return  (
                <div className="container">
                    <div className="row">
                        <div className="col s12">
                            <CircularProgress className={classes.progress} style={{ color: green[500] }} thickness={7} />
                        </div>
                    </div>
                </div>
            )
         }else if(this.state.aanvraagCompleet === undefined){
            return(
                <div className="container">
                {!this.isAuthenticated() ? <Redirect to={{pathname : '/login'}} />: (
                    <div className="row">
                        <div className="col s2">                        
                        </div>
                        <div className="col s8">
                            <h1 className={classes.titel}>AanvraagID: {aanvraag.id}</h1>
                            <div className="row">
                                <div className="col s6">
                                    <h2 className={classes.subTitel}>Aanvrager gegevens:</h2>
                                    <p>Naam: {aanvraag.naam}</p>
                                    <p>Email: {aanvraag.email}</p>
                                    <p>Divisie: {aanvraag.divisie}</p>
                                    <p>Afdeling: {aanvraag.afdeling}</p>
                                    <p>Team: {aanvraag.team}</p>
                                </div>
                                <div className="col s6">
                                    <h2 className={classes.subTitel}>Project gegevens:</h2>
                                    <p>Projectnaam: {aanvraag.project.naam}</p>
                                    <p>Project lead naam: {aanvraag.project.lead_naam}</p>
                                    <p>Project lead email: {aanvraag.project.lead_email}</p>
                                    <p>Project type: {aanvraag.project.type}</p>
                                </div>
                            </div>
                            <h2 className={classes.subTitel}>Project gegevens:</h2>
                           <div className="row">
                                <div className="col s4">
                                    <p className={classes.bold}>Naam:</p>
                                    <ul className={classes.ul}>
                                        {this.renderTeamlidNaam()}
                                    </ul>
                                </div>
                                <div className="col s4">
                                    <p className={classes.bold}>Email:</p>
                                    <ul className={classes.ul}>
                                        {this.renderTeamlidEmail()}
                                    </ul>
                                </div>
                                <div className="col s4">
                                    <p className={classes.bold}>Adminrechten:</p>
                                    <ul className={classes.ul}>
                                       {this.renderTeamlidRechten()}
                                    </ul>
                                </div>
                            </div>
                             {aanvraag.status === 'open' ? (
                                <div>
                                <MyContext.Consumer>
                                    {(context) => (
                                        <React.Fragment>
                                            <Button 
                                                variant="contained" 
                                                color="primary" 
                                                size="large" 
                                                className={classes.button} 
                                                onClick={() => this.verwerkAanvraag(context.state.name)}
                                            >
                                            ACCEPTEER
                                            </Button>
                                        </React.Fragment>
                                    )}
                                </MyContext.Consumer>
                            <Button variant="contained" color="primary" size="large" className={classes.button} onClick={this.handleOpen}>
                                WEIGER
                             </Button></div>
                        ) : (
                          ''
                        )}
                        </div>
                        <div className="col s2"></div>
                        <div className="row">
                            <div className="col s2"></div>
                            <div className="col s8">
                                <Modal
                                    aria-labelledby="simple-modal-title"
                                    aria-describedby="simple-modal-description"
                                    open={this.state.open}
                                    onClose={this.handleClose}
                                >
                                <div style={getModalStyle()} className={classes.paper}>
                                <div className="row">
                                        <p>Geef een duidelijke reden voor afwijzing.</p>
                                        <textarea rows="4" cols="40" onChange={this.handleChange('afwijzing')} name="afwijzing" value={this.state.afwijzing}>
                                        </textarea>
                                        <Button variant="contained" color="primary" size="large" className={classes.button} onClick={() =>  this.compleetAanvraag(aanvraag.id,"weiger")}>
                                            Verzend
                                        </Button>
                                </div>
                                </div>
                                </Modal>
                            </div>
                            <div className="col s2"></div>
                        </div>
                    </div>
                    
                )}
                </div>
            )
         }
         
     }
 }
 
 //mutation om de aanvraag aan te passen
 const mutation = 
 gql`mutation editAanvraag($id:ID, $verwerkt_door:String,$verwerkt_op:String){
     editAanvraag(id:$id,verwerkt_door:$verwerkt_door,verwerkt_op:$verwerkt_op){
       id
     }
   }`;
 
 //export
 export default withStyles(style)(graphql(mutation)(graphql(FetchDetail, {
     options: (props) => {return {variables: {id: props.match.params.id}}}
 })(withCookies(AanvraagDetail))));
 