/**
 * Agile portal, Een webportal ontwikkeld door IT4IT&Testmanagement
 * Copyright (C) 2018, SSC-ICT
 * mailto: agile@minbzk.nl
 */

 //imports
import React from 'react';
import {graphql} from 'react-apollo';
import {Link, Redirect} from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import FetchAanvraag from '../../queries/fetchaanvraag';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import Search from '@material-ui/icons/Search';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import NativeSelect from '@material-ui/core/NativeSelect';
import CircularProgress from '@material-ui/core/CircularProgress';
import green from '@material-ui/core/colors/green';
import { withCookies, Cookies } from 'react-cookie';
import { instanceOf } from 'prop-types';
import {MyContext} from '../../app';

//CSS styles
const styles = theme => ({
    card: {
      minWidth: 275,
    },
    label:{
        fontSize: 15,
        color:'black'
    },
    bullet: {
      display: 'inline-block',
      margin: '0 2px',
      transform: 'scale(0.8)',
    },
    title: {
      fontSize: 14,
    },
    pos: {
      marginBottom: 12,
    },
    status:{
        float:'right'
    },
    datum:{
        fontSize:8,
        color:'#39870c',
        marginBottom:5
    },
    projectNaam:{
        fontWeight:'bold',
        fontSize:15
    },
    CardContent:{
        lineHeight:0.8
    },
    margin: {
        margin: theme.spacing.unit,
    },
    TextField:{
        margin: theme.spacing.unit,
        width:'100%'
    },
    resize:{
        fontSize: 13
    },
    formControl: {
        margin: theme.spacing.unit,
        width:'100%',
        marginTop: 20
    },
    selectEmpty: {
        marginTop: theme.spacing.unit * 2,
    },
    progress: {
        display:'block',
        margin: 'auto',

    },
    newFontSize:{
        fontSize:'40px !important'
    }
  });




class Overzicht extends React.Component{
    
    static propTypes = {
        cookies: instanceOf(Cookies).isRequired
    };

    constructor(props, context){
        super(props);
        this.state = {
            filter: 'open aanvragen',
            sorteer: 'datum',
            zoek: '',
        };
    }

    componentDidMount(){
       this.props.data.refetch();
    }

    /**
     * Een array met gefilterde aanvragen wordt hier naar toe gestuurd. 
     * Vervolgens met een map() ga ik over de array heen en display ik de data in JSX.
     */
    filterAanvragen(aanvragen){
        const {classes} = this.props;
        return aanvragen.map(({id, naam,status,datum,project, verwerkt_door, verwerkt_op}) => {
            return(
                <div className="row" key={id}>
                    <div className="col s2"></div>
                    <div className="col s8">
                         <Card className={classes.card}>
                            <CardContent className={classes.CardContent}>
                                <div className={classes.status}>Status: {status}{status === 'gesloten' ? <div><p>Verwerkt op: {verwerkt_op}</p><p>Verwerkt door: {verwerkt_door}</p></div> : '' }</div>
                                <p className={classes.datum}>{datum}</p>
                                <p className={classes.projectNaam}>{project.naam}</p>
                                <p>AanvraagID: {id}</p>
                                <p>Aanvragen: {naam}</p>
                            </CardContent>
                            <hr/>
                            <CardActions>
                                <Link to={`/aanvraag/${id}`}>
                                    <Button size="medium" variant="contained" color="primary">Details</Button>
                                </Link>
                            </CardActions>
                        </Card>
                    </div>
                    <div className="col s2"></div>
                </div>
            );
        });
    }

    /**
     * renderAanvragen is verantwoordelijk voor het bepalen van welke aanvragen gezien worden
     */
    renderAanvragen(){
        
        //als er geen aanvragen zijn of elke aanvraag is gesloten dan krijg je deze melding. 
        if(!this.props.data.aanvragen){
            return <p>Geen aanvragen.</p>
        }

        /**
         * Als de gebruiker iets heeft ingetypt in de searchbar dan krijgen we alle aanvragen
         * waarvan de project naam this.state.zoek bevat.
         */
        if(this.state.zoek.length > 0){
                var zoekresultaten = this.props.data.aanvragen.filter(x => x.project.naam.includes(this.state.zoek));
                return this.filterAanvragen(zoekresultaten);
        }else{
            /**
             * Er wordt een gefilterd array gestuurd naar filterAanvragen op basis van de state filter
             * en de status van een aanvraag. 
             */
            switch(this.state.filter){
                case "gesloten aanvragen":
                        var gesloten = this.props.data.aanvragen.filter(x => x.status === 'gesloten');
                        return this.filterAanvragen(gesloten);
                case "open aanvragen":
                        var open = this.props.data.aanvragen.filter(x => x.status === 'open');
                        return this.filterAanvragen(open);
                case "alle aanvragen":
                        return this.filterAanvragen(this.props.data.aanvragen);
            } 
        }
    }

    //checkt of gebruiker is ingelogd
    isAuthenticated(){
        const { cookies } = this.props;
        const token = cookies.get('token');
        if(token && token.length > 10){
            return true;
        }else{
            return false;
        }
    }
 
    logUit(){
        const { cookies } = this.props;
        cookies.remove('token');
    }

    //generic onChange handler
    handleChange = name => event => {
        this.setState({ [name]: event.target.value });
    };

    setTextFilter = (event) => {
        this.setState({zoek: event});
    }

    render(){
        const {classes} = this.props;        
        if(this.props.data.loading){return  (
            <div className="container">
                <div className="row">
                    <div className="col s12">
                        <CircularProgress className={classes.progress} style={{ color: green[500] }} thickness={7} />
                    </div>
                </div>
            </div>
        )}
        return(
         <div>
            {!this.isAuthenticated() ? <Redirect to={{pathname : '/login'}} />: (
            
                <div>
                    <div className="container">
                        <div className="row">
                           <div className="col s6">
                           <MyContext.Consumer>
                                {(context) => (
                                <React.Fragment>
                                    <p>Welom terug {context.state.name}</p>
                                </React.Fragment>
                                )}
                            </MyContext.Consumer>
                             <Button onClick={() => this.logUit()} 
                                    variant="contained" 
                                    color="primary" 
                            >
                                Log uit
                            </Button>
                           </div>
                        </div>
                        <div className="row">
                            <div className="col s6">
                                <div className={classes.margin}>
                                    <TextField
                                        className={classes.TextField}
                                        id="input-with-icon-textfield"
                                        label="Zoeken"
                                        InputProps={{
                                        startAdornment: (
                                            <InputAdornment position="start">
                                            <Search />
                                            </InputAdornment>
                                        ),
                                        classes: {
                                            input: classes.resize                                        }
                                        }}
                                        onChange={(e) => {this.setTextFilter(e.target.value);}}
                                    />
                                </div>
                            </div>
                            <div className="col s3">
                                <FormControl className={classes.formControl}>
                                    <InputLabel shrink htmlFor="age-native-label-placeholder" className={classes.label}>
                                        Filter
                                    </InputLabel>
                                    <NativeSelect
                                        value={this.state.filter}
                                        onChange={this.handleChange('filter')}
                                        input={<Input name="filter" id="age-native-label-placeholder" />}
                                    >
                                        <option value="open aanvragen" >Open aanvragen</option>
                                        <option value="gesloten aanvragen">Gesloten aanvragen</option>
                                        <option value="alle aanvragen">Alle aanvragen</option>
                                    </NativeSelect>
                                </FormControl>
                            </div>
                            <div className="col s3">
                                <FormControl className={classes.formControl}>
                                    <InputLabel shrink htmlFor="age-native-label-placeholder" className={classes.label}>
                                        Sorteer
                                    </InputLabel>
                                    <NativeSelect
                            
                                        value={this.state.sorteer}
                                        onChange={this.handleChange('sorteer')}
                                        input={<Input name="sorteer" id="age-native-label-placeholder" />}
                                    >
                                        <option value="Datum" >Datum</option>
                                        <option value="Aanvrager" >Aanvrager naam</option>
                                    </NativeSelect>
                                </FormControl>
                            </div>
                        </div>
                    </div>
                    <div className="container">
                        {this.renderAanvragen()}
                    </div>
                </div>  
            )}
         </div>
        );
    }
};

//export
export default withStyles(styles)(graphql(FetchAanvraag)(withCookies(Overzicht)));;


