import React, {Component} from 'react';
import Login from './AppLogin';
import AanvragenOverzicht from './AppAanvraagOverzicht';
import { withCookies, Cookies } from 'react-cookie';
import { instanceOf } from 'prop-types';

// first we will make a new context


class Admin extends React.Component{

    static propTypes = {
        cookies: instanceOf(Cookies).isRequired
    };

     //checkt of gebruiker is ingelogd
     isAuthenticated(){
        const { cookies } = this.props;
        const token = cookies.get('token');
        if(token && token.length > 10){
            return true;
        }else{
            return false;
        }
    }

    constructor(props){
        super(props);
    }

    render(){
        return(
            <React.Fragment>
                {!this.isAuthenticated() ? 
                     <Login/> 
                : 
                    <AanvragenOverzicht/>
                }
            </React.Fragment>
        );
    }
}

export default withCookies(Admin);