/**
 * Agile portal, Een webportal ontwikkeld door IT4IT&Testmanagement
 * Copyright (C) 2018, SSC-ICT
 * mailto: agile@minbzk.nl
 */

 //imports
import React from 'react';
import LoginForm from './AppLoginForm';
import {Redirect} from 'react-router-dom';
import { withCookies, Cookies } from 'react-cookie';
import { instanceOf } from 'prop-types';


class Login extends React.Component{

    static propTypes = {
        cookies: instanceOf(Cookies).isRequired
    };

    //checkt of gebruiker is ingelogd
    isAuthenticated(){
        const { cookies } = this.props;
        const token = cookies.get('token');
        if(token && token.length > 10){
            return true;
        }else{
            return false;
        }
    }
  

    handelSuccesLogin(){
        this.setState();
    }

    render(){
        return(
            <div>
                {this.isAuthenticated() ? <Redirect to={{pathname : '/aanvragen-overzicht'}} />: (  
                    <div>
                        <LoginForm/>
                    </div>)
                }
            </div>
          
        );
    }
}

export default withCookies(Login);