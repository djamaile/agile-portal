/**
 * Agile portal, Een webportal ontwikkeld door IT4IT&Testmanagement
 * Copyright (C) 2018, SSC-ICT
 * mailto: agile@minbzk.nl
 */

 //imports 
import React, { Component } from 'react';
import { instanceOf } from 'prop-types';
import { withCookies, Cookies } from 'react-cookie';
import { withStyles } from '@material-ui/core/styles';
import axios from 'axios';
import Paper from '@material-ui/core/Paper';
import {Redirect} from 'react-router-dom';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';
import AccountCircle from '@material-ui/icons/AccountCircle';
import Lock from '@material-ui/icons/Lock';
import Button from '@material-ui/core/Button';
import {MyContext} from '../../app';




//CSS styles
const styles = theme => ({
    root: {
      ...theme.mixins.gutters(),
      paddingTop: theme.spacing.unit * 2,
      paddingBottom: theme.spacing.unit * 2,
      marginTop: 100
    },
    margin: {
        margin: theme.spacing.unit,
        width: '100%'
    },
    button: {
        margin: theme.spacing.unit,
    },
    input: {
        display: 'none',
    },
    resize:{
        fontSize: 13
    },
    error:{
        color:'red'
    }
  });

class LoginForm extends Component {

   static propTypes = {
        cookies: instanceOf(Cookies).isRequired
    };

  constructor(props){
    super(props);
    this.state = {
        username: '',
        password: '',
        error: false
    };
  }



  //generic onChange
  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  /**
   * Als de gebruiker heeft ingelogd dan wordt er informatie gestuurd naar de backend
   * Als ik een goede response krijg, sla ik de sessie op in een cookie
   * Met de cookie check ik ook de heletijd of een gebruiker is ingelogd ja of nee
   */
  onSubmit = e => {
    e.preventDefault();
    const { cookies } = this.props;
    var login= {
        "username": this.state.username,
        "password": this.state.password
    };
    axios.post('/api/login', login)
            .then(resp => {
                if(resp.status === 200){
                    var value = Math.random().toString(36).substring(2,15);
                    cookies.set('token', value, { path: '/', expires: 0});
                }else{
                    this.setState({error: true});
                }
            })
            .catch(err => {
                console.log(err);
                this.setState({error: true});
            });
  }

  //check of gebruiker is ingelogd 
  isAuthenticated(){
      const { cookies } = this.props;
      const token = cookies.get('token');
      if(token && token.length > 10){
          return true;
      }else{
          return false;
      }
  }

  render() {
    const { classes } = this.props;
    return (
        <div>
            {this.isAuthenticated() ? <Redirect to={{pathname : '/aanvragen-overzicht'}} />: 
               <div>
                    <div className="container">
                        <div className="row">
                            <div className="col s2"></div>
                            <div className="col s8">
                                <Paper className={classes.root} elevation={24}>
                                    <form onSubmit={this.onSubmit}>
                                        {this.state.error === true ? <p className={classes.error}>Inloggen niet gelukt</p> : ''}
                                        <div className="row">
                                            <div className="col s12">
                                                <FormControl className={classes.margin}>
                                                    <InputLabel htmlFor="input-with-icon-adornment-1" className={classes.resize}>Email:</InputLabel>
                                                    <Input
                                                    id="input-with-icon-adornment-1"
                                                    startAdornment={
                                                        <InputAdornment position="start">
                                                        <AccountCircle />
                                                        </InputAdornment>
                                                    }
                                                    name="username"
                                                    onChange={this.handleChange}
                                                    value={this.state.username}
                                                    className={classes.resize}
                                                    error={this.state.error}
                                                    />
                                                </FormControl>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col s12">
                                                <FormControl className={classes.margin}>
                                                    <InputLabel htmlFor="input-with-icon-adornment-2" className={classes.resize}>Wachtwoord:</InputLabel>
                                                    <Input
                                                    id="input-with-icon-adornment-2"
                                                    startAdornment={
                                                        <InputAdornment position="start">
                                                        <Lock />
                                                        </InputAdornment>
                                                    }
                                                    name="password"
                                                    onChange={this.handleChange}
                                                    value={this.state.password}
                                                    className={classes.resize}
                                                    error={this.state.error}
                                                    type="password"
                                                    />
                                                </FormControl>
                                            </div>
                                        </div>
                                        <MyContext.Consumer>
                                            {(context) => (
                                            <React.Fragment>
                                                <Button variant="contained" color="primary" className={classes.button} type="submit" onClick={() => context.setUsername(this.state.username)}>
                                                    Login
                                                </Button>
                                            </React.Fragment>
                                            )}
                                        </MyContext.Consumer>
                                    </form>
                                </Paper>
                            </div>
                            <div className="col s2"></div>
                        </div>
                    </div>  
               </div>
            }
        </div>
    );
  }
}

//export
export default withStyles(styles)(withCookies(LoginForm));