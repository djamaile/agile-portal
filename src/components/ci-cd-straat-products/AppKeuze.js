/**
 * Agile portal, Een webportal ontwikkeld door IT4IT&Testmanagement
 * Copyright (C) 2018, SSC-ICT
 * mailto: agile@minbzk.nl
 */

 //imports
import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import JiraKlein from '../../../public/images/jiraklein.png'; 
import ConfluenceKlein from '../../../public/images/confluenceklein.png';
import BitbucketKlein from '../../../public/images/bitbucketklein.png';
import Sonarqubeklein from '../../../public/images/sonarqubeklein.png';
import Product from './AppStraatProduct';
import Welkom from './AppWelcome';

//CSS style
const styles = theme => ({
    root: {
      flexGrow: 1,
    },
  });

  /**
   * 
   * CI/CD-straat componenten laat ik zien hier, doormiddel van Product.
   * Als je een component wilt toevoegen, gewoon product tag toevoegen met de juiste props
   */
  const AllComponenten = (props) => {
      const {classes} = props;
      return(
        <div className={classes.root}>
           <Welkom
               titel="Agile portal"
               subtekst="Kies één van de componenten uit en maak gebruik van de Agile portal"
           />
           <div className="container">
               <div className="row">
                    <div className="col s12 m3 lg3">
                        <Product
                            foto={JiraKlein}
                            subtekst="Hier kan u al uw Jira project aanvragen doen"
                            link="/jira"
                            disabled={false}
                        />
                    </div>
                    <div className="col s12 m3 lg3">
                        <Product
                            foto={ConfluenceKlein}
                            subtekst="Beste medewerker hier wordt nog aangewerkt"
                            link="/"
                            disabled={true}
                        />
                    </div>
                    <div className="col s12 m3 lg3">
                        <Product
                            foto={BitbucketKlein}
                            subtekst="Beste medewerker hier wordt nog aangewerkt"
                            link="/"
                            disabled={true}
                        />
                    </div>
                    <div className="col s12 m3 lg3">
                        <Product
                            foto={Sonarqubeklein}
                            subtekst="Beste medewerker hier wordt nog aangewerkt"
                            link="/"
                            disabled={true}
                        />
                    </div>
               </div>
           </div>
         </div>
     );
  }; 

  AllComponenten.propTypes = {
    classes: PropTypes.object.isRequired,
  };

  //export
  export default withStyles(styles)(AllComponenten);
