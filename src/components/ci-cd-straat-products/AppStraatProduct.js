/**
 * Agile portal, Een webportal ontwikkeld door IT4IT&Testmanagement
 * Copyright (C) 2018, SSC-ICT
 * mailto: agile@minbzk.nl
 */

 //imports
import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import {Link} from 'react-router-dom';

//CSS styles
const styles = {
  card: {
    maxWidth: 345,
    marginBottom: 25,
    marginTop: 50
  },
  media: {
   
  },
};

/**
 * 
 * Generieke product const om de atlassian componenten in te laden.
 * Linkt werkt hier niet in IE, wel in Chrome. Oplossing? 
 */
const Product = (props) => {
  const { classes, foto,subtekst,link,disabled} = props;
  const myLink = props => <Link to={link} {...props}/>
  return (
    <Card className={classes.card} elevation={24}>
      <CardActionArea>
        <Link to={link}>
            <CardMedia
            component="img"
            alt="Product"
            className={classes.media}
            image={foto}
            />
        </Link>
        <CardContent>
          <Typography component="p">
            {subtekst}
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>
        <Button size="small" color="primary" variant='contained' disabled={disabled} component={myLink}>
                    Klik hier
        </Button>  
        <Button size="small" color="primary" variant='contained' disabled={disabled}>
                    Guide
        </Button>     
      </CardActions>
    </Card>
  );
}

Product.propTypes = {
  classes: PropTypes.object.isRequired,
};

//exports
export default withStyles(styles)(Product);