/**
 * Agile portal, Een webportal ontwikkeld door IT4IT&Testmanagement
 * Copyright (C) 2018, SSC-ICT
 * mailto: agile@minbzk.nl
 */

 //imports
import React from 'react';
import { withStyles } from '@material-ui/core/styles';

//CSS style
const styles = theme => ({
    welkomTekst:{
        textAlign: 'center',
        color:'#6cb842',
        fontSize:72,
        fontWeight: 400,
        lineHeight: 0.2
    },
    welkomSubtekst:{
        textAlign: 'center',
        fontSize:18,
        color: '#6cb842',
        fontStyle: 'italic'
    }
  });

// Generieke welkom titel, zodat ik het niet op elke pagina hoef te herhalen. 
const Welkom = ({titel, classes, subtekst}) => {
    
    return(
        <div>
             <h1 className={classes.welkomTekst}>{titel}</h1>
              <p className={classes.welkomSubtekst}>{subtekst}</p>
        </div>
    );
};

//export
export default withStyles(styles)(Welkom);