/**
 * Agile portal, Een webportal ontwikkeld door IT4IT&Testmanagement
 * Copyright (C) 2018, SSC-ICT
 * mailto: agile@minbzk.nl
 */

 //imports
import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import {Link} from 'react-router-dom';

//CSS styles
const styles = theme => ({
    card: {
      marginRight: 25,
      marginLeft: 25,
      minWidth: 275,
    },
    bullet: {
      display: 'inline-block',
      margin: '0 2px',
      transform: 'scale(0.8)',
    },
    title: {
      marginBottom: 16,
      fontSize: 14,
    },
    pos: {
      marginBottom: 12,
    },
    cardTitle:{
      fontSize: 24,
      marginBottom:5
    },
    cardText:{
      fontSize:15
    },
    cardHR:{
      height: 0,
      border: 'none',
      borderTop: '1px solid black'
    },
    Button:{
      textDecoration:'none'
    },
    Links:{
      textDecoration: 'none',
      '&:hover, &:focus, &:visited, &:link, &:active':{
        textDecoration:'none'
      }
    }
});

/**
 * Actie is een generic card die naar een bepaalde pagina leidt. 
 * Bijvoorbeeld naar een project aanvragen of een account aanvragen.
 */
const Actie = (props) => {
    const {classes,titel,beschrijving,buttonTekst,link,disabled} = props;
    return(
      <div>
        <Card className={classes.card} elevation={12}>
          <CardContent>
            <Typography variant="headline" component="h2" className={classes.cardTitle}>
              {titel}
            </Typography>
            <Typography component="p" className={classes.cardText}>
                {beschrijving}
            </Typography>
          </CardContent>
          <hr className={classes.cardHR}/>
          <CardActions>
            {disabled === true ?   
                    <Button 
                    size="large" 
                    elevation={24} 
                    variant="contained" 
                    color="primary" 
                    className={classes.Button}
                    disabled={disabled}
                    >
                    {buttonTekst}
                    </Button> 
                    : 
                    <Link to={link} className={classes.Links}>
                      <Button 
                            size="large" 
                            elevation={24} 
                            variant="contained" 
                            color="primary" 
                            className={classes.Button}
                            disabled={disabled}
                        >
                            {buttonTekst}
                        </Button>
                     </Link> 
                }
          </CardActions>
         </Card>
      </div>
        
    );
};

Actie.propTypes = {
    classes: PropTypes.object.isRequired,
};

//export
export default withStyles(styles)(Actie);
