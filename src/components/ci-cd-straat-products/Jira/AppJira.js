/**
 * Agile portal, Een webportal ontwikkeld door IT4IT&Testmanagement
 * Copyright (C) 2018, SSC-ICT
 * mailto: agile@minbzk.nl
 */

 //imports
import React from 'react';
import Grid from '@material-ui/core/Grid';
import Welkom from '../AppWelcome';
import { withStyles } from '@material-ui/core/styles';
import {Link} from 'react-router-dom';
import Actie from './AppActie';

//CSS Styles
const styles = theme => ({
    root: {
      flexGrow: 1,
      marginBottom: 192.5
    }
});

/**
 * Actie wordt drie keer ingeladen 
 * Elke actie betreft een Jira acitviteit. 
 * Jeetje jongens wat ben ik moe.  
 */
const Jira = (props) => {
    const {classes} = props;
    return(
        <div className={classes.root}>
           <Welkom titel="Jira" subtekst="Project aanvragen? Nieuwe account aanvragen? Dat kan hier allemaal."/>
           <Grid container spacing={24}>
                <Grid item sm={12} xs={12} md={4} lg={4}>
                    <Actie
                        titel="Project"
                        beschrijving="Hier kan je een project aanvragen"
                        buttonTekst="Project aanvragen"
                        disabled={false}
                        link="/project-aanvraag"
                    />
                </Grid>
                <Grid item sm={12} xs={12} md={4} lg={4}>
                    <Actie
                        titel="Account"
                        beschrijving="Hier kan je een Jira account aanvragen"
                        buttonTekst="Account aanvragen"
                        link="/account-aanvraag"
                    />
                </Grid>
                <Grid item sm={12} xs={12} md={4} lg={4}>
                    <Actie
                        titel="Guide"
                        beschrijving="Hier kan je de guide lezen"
                        buttonTekst="Guide lezen"
                        disabled={true}
                    />
                </Grid>
           </Grid>
        </div>
    );
};

//export
export default withStyles(styles)(Jira);