/**
 * Agile portal, Een webportal ontwikkeld door IT4IT&Testmanagement
 * Copyright (C) 2018, SSC-ICT
 * mailto: agile@minbzk.nl
 */

//imports
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';

//CSS Styles
const styles = theme => ({
    titel:{
        fontSize: 72,
        textAlign: 'center',
        color: '#39870c',
        lineHeight: 0.1
    },
    subtekst:{
        textAlign: 'center',
        fontSize: 18,
        color: 'black',
        fontStyle: 'italic'
    },
    id:{
        fontWeight:500,
        color:'black'
    }
})

/**
 * Bedankt scherm die tevoorschijn komt als de formulier comleet is ingevuld. 
 */
class Bedankt extends React.Component{
    render(){
        const {classes} = this.props;

        return(
            <div className="row">
                <div className="col s12">
                    <h3 className={classes.titel}>Bedankt!</h3>
                    <p className={classes.subtekst}>
                    Uw aanvraag wordt binnen twee werkdagen in behandeling genomen. <br/>
                    Voor vragen stuur een email naar agile@minbzk.nl</p>
                </div>
            </div>
        );
    };
};

Bedankt.propTypes = {
    classes: PropTypes.object.isRequired,
};

//export
export default withStyles(styles)(Bedankt);