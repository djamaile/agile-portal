/**
 * Agile portal, Een webportal ontwikkeld door IT4IT&Testmanagement
 * Copyright (C) 2018, SSC-ICT
 * mailto: agile@minbzk.nl
 */

 //imports
import React from 'react';
import { withStyles } from '@material-ui/core/styles';

//CSS styles
const styles = theme => ({
    titel: {
        fontSize: 21, 
        color: '#005900',
        fontWeight:400,
        textAlign: 'left'
    }
});

/*
    Generieke form titel
 */
const FormTitle = (props) => {
    const {classes, titel} = props;
    return(
        <div className="container">
            <div className="row">
                <div className="col s12 m12 l12">
                    <h3 className={classes.titel}>{titel}</h3>
                </div>
            </div>
        </div>
    );
};

//exports
export default withStyles(styles)(FormTitle)