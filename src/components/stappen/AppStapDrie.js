/**
 * Agile portal, Een webportal ontwikkeld door IT4IT&Testmanagement
 * Copyright (C) 2018, SSC-ICT
 * mailto: agile@minbzk.nl
 */

//imports
import React from 'react';
import Titel from './AppFormTitle';
import { withStyles } from '@material-ui/core/styles';

const styles = {
    lineHeight: 1
}
const row = {
    marginBottom: 55
}

/**
 * Dit is de confermatie scherm, Alle waardes van wordt van state afgelezen en hier getoond.
 * Gebruik destructering als je meer waardes wilt toevoegen. 
 * @param {*} param0 
 */
const Stap = ({naam, email,divisie, afdeling, team, pjnaam, pjlnaam,pjlemail,pjtype,
teamledennaam,teamledenemail, teamledenrechten}) => {
    return(
    <div>
    <Titel titel="Bevestiging van uw projectaanvraag"/>
    <div className="container">
        <div className="row">
            <div className="col s4 blokje-gegevens">
                <h4 style={styles}>Persoonlijke <br/>gegevens</h4>
                <p>Naam: {naam}</p>
                <p>Email: {email}</p>
                <p>Divisie: {divisie}</p>
                <p>Afdeling: {afdeling}</p>
                <p>Team: {team}</p>
            </div>
            <div className="col s4 blokje-gegevens">
                <h4 style={styles}>Project gegevens</h4>
                <p>Projectnaam: {pjnaam}</p>
                <p>Projectlead naam: {pjlnaam}</p>
                <p>Projectlead email: {pjlemail}</p>
                <p>Project type: {pjtype}</p>
            </div>
        </div>
        <div className="row" style={row}>
            <div className="col s4 blokje-gegevens">
                <h4 style={styles}>Teamlid namen</h4>
                {teamledennaam}
            </div>
            <div className="col s4 blokje-gegevens">
                <h4 style={styles}>Teamlid emails</h4>
                {teamledenemail}
            </div>
            <div className="col s4 blokje-gegevens">
                <h4 style={styles}>adminrechten</h4>
                {teamledenrechten}
            </div>
        </div>
    </div>
    </div>
    );
};

//export
export default withStyles(styles)(Stap);