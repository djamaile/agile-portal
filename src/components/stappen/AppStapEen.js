/**
 * Agile portal, Een webportal ontwikkeld door IT4IT&Testmanagement
 * Copyright (C) 2018, SSC-ICT
 * mailto: agile@minbzk.nl
 */

 //imports
import React from 'react';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import Afdeling from './selects/AppAfdeling';
import Team from './selects/AppTeam';

//CSS style
const styles = theme => ({
    container: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    row:{
        marginBottom:0
    },
    textField: {
      marginLeft: theme.spacing.unit,
      marginRight: theme.spacing.unit,
      width: '100%',
      paddingBottom: 15,
    },
    dense: {
      marginTop: 19,
    },
    menu: {
      width: 200,
    },
    formControl: {
        margin: theme.spacing.unit,
        minWidth: 120,
        width: '100%',
        fontSize: 15,
        paddingBottom: 15
      },
      selectEmpty: {
        marginTop: theme.spacing.unit * 2,
      },
      resize:{
        fontSize:13
      },
  });

/**
 * Eerste stap van het formulier. Alle input velden zijn geimport van Material-UI.
 * Alle inputs hebben ook props values en pros onChange's. Om dit te kunnen aanpassen
 * graag kijken binnen AanvraagFormulier.js
 */
class Stap extends React.Component {
    constructor(props){
        super(props);
    }
    
    render(){
        const {classes} = this.props;
        return(
            <React.Fragment>
            <div className={classNames("row", classes.row)}>
                <div className="col s6">
                    <TextField
                    name="naam"
                    id="standard-dense"
                    label="Volledige naam"
                    value={this.props.naamValue}
                    maxLength="255"
                    className={classNames(classes.textField, classes.dense)}
                    margin="dense"
                    onChange={this.props.onNaamChange}
                    InputProps={{
                        classes: {
                        input: classes.resize,
                        },
                    }}
                    error={
                        this.props.naamError === true ? 
                        true:false
                    }
                    />
                </div>
                <div className="col s6">
                    <TextField
                    id="standard-dense"
                    name="email"
                    label="E-mail"
                    value={this.props.emailValue}
                    maxLength="255"
                    onChange={this.props.onEmaileChange}
                    className={classNames(classes.textField, classes.dense)}
                    margin="dense"
                    helperText={this.props.emailError === true && this.props.emailValue.length > 0 ? "Enkel een Rijks e-mailadres is toegestaan" : ''}
                    InputProps={{
                        classes: {
                        input: classes.resize,
                        },
                    }}
                    error={
                        this.props.emailError === true ? 
                        true:false
                    }
                    />
                </div>
            </div>
            <div className={classNames("row", classes.row)}>
             <div className="col s6">
                        <FormControl 
                        className={classes.formControl}  
                        error={
                        this.props.divisieError === true ? 
                        true:false
                        }>
                                <InputLabel htmlFor="Divisie">Divisie</InputLabel>
                                <Select
                                    value="divisie"
                                    name="divisie"
                                    className={classes.selectEmpty}
                                    value={this.props.SelectDivisieValue}
                                    onChange={this.props.handleDivisieChange}
                                >
                                
                                <MenuItem value="Services">Services</MenuItem>
                                <MenuItem value="Applicaties">Applicaties</MenuItem>
                                <MenuItem value="Infrastructuur">Infrastructuur</MenuItem>
                                <MenuItem value="Bedrijfsvoering">Bedrijfsvoering</MenuItem>
                            </Select>
                        </FormControl>
                    </div>
                    <div className="col s6">
                        <FormControl 
                            className={classes.formControl}  
                            error={
                            this.props.afdelingError === true ? 
                            true:false}
                        >
                                <Afdeling
                                    value={this.props.SelectDivisieValue}
                                    SelectAfdelingValue={this.props.SelectAfdelingValue}
                                    handleAfdelingChange={this.props.handleAfdelingChange}
                                />
                        </FormControl>
                    </div>
            </div>

            <div className="row">
            <div className="col s6">
                        <FormControl 
                        className={classes.formControl}  
                        error={
                        this.props.teamError === true ? 
                        true:false
                        }>
                            <Team
                                value={this.props.SelectAfdelingValue}
                                SelectTeamValue={this.props.SelectTeamValue}
                                handleTeamChange={this.props.handleTeamChange}
                            />
                        </FormControl>
                    </div>
                    <div className="col s6">
                        <FormControl 
                        className={classes.formControl}  
                        error={
                        this.props.projectLeadKeuzeError === true ? 
                        true:false
                        }>
                                <InputLabel htmlFor="aanvragerIsProjectLead">Bent u project lead?</InputLabel>
                                <Select
                                    name="aanvragerIsProjectLead"
                                    className={classes.selectEmpty}
                                    value={this.props.aanvragerIsProjectLead}
                                    onChange={this.props.handleProjectLeadKeuzeChange}
                                >
                                
                                <MenuItem value="Ja">Ja</MenuItem>
                                <MenuItem value="Nee">Nee</MenuItem>
                            </Select>
                        </FormControl>
                    </div>
            </div>
            </React.Fragment>
                    
        );
    };
};

//export
export default withStyles(styles)(Stap);
