/**
 * Agile portal, Een webportal ontwikkeld door IT4IT&Testmanagement
 * Copyright (C) 2018, SSC-ICT
 * mailto: agile@minbzk.nl
 */

//imports 
import React from 'react';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import Input from '@material-ui/core/Input';

//CSS style
const styles = theme => ({
    container: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    row:{
        marginBottom:0
    },
    textField: {
      marginLeft: theme.spacing.unit,
      marginRight: theme.spacing.unit,
      width: '100%',
      paddingBottom: 15
    },
    dense: {
      marginTop: 19,
    },
    menu: {
      width: 200,
    },
    formControl: {
        margin: theme.spacing.unit,
        minWidth: 120,
        width: '100%',
        marginBottom: 15
      },
      selectEmpty: {
        marginTop: theme.spacing.unit * 2,
      },
      InputLabel:{
          fontSize: 15
      },
      resize:{
        fontSize:13
      },
  });

/**
 * Tweede stap van het formulier. Alle input velden zijn geimport van Material-UI.
 * Alle inputs hebben ook props values en pros onChange's. Om dit te kunnen aanpassen
 * graag kijken binnen AanvraagFormulier.js
*/
class Stap extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        const {classes} = this.props;       
        return(
            <div className="row">
                <div className="col s12">
                    <TextField
                    name="projectnaam"
                    id="standard-dense"
                    label="Projectnaam"
                    className={classNames(classes.textField, classes.dense)}
                    margin="dense"
                    value={this.props.projectNaamValue}
                    maxLength="255"
                    onChange={this.props.onProjectNaamChange}
                    InputProps={{
                        classes: {
                        input: classes.resize,
                        },
                    }}
                    error={
                        this.props.pjnaamError === true ? 
                        true:false
                    }/>
                </div>
                <div className="col s12">
                    <FormControl className={classes.formControl} 
                        error={
                        this.props.projecttypeError === true ? 
                        true:false
                    }
                    >
                            <InputLabel htmlFor="Divisie">Project type</InputLabel>
                            <Select
                                value="Ok"
                                input={<Input name="projecttype" id="age-label-placeholder" />}
                                displayEmpty
                                name="ptype"
                                className={classes.selectEmpty}
                                value={this.props.projectTypeValue}
                                onChange={this.props.onprojectTypeChange}
                            >
                            <MenuItem value="Scrum">Scrum</MenuItem>
                            <MenuItem value="Kanban">Kanban</MenuItem>
                        </Select>
                    </FormControl>
                </div>
                
                <div className="col s6">
                    <TextField
                    id="standard-dense"
                    name="pjnaam"
                    label="Project Lead  - volledige naam"
                    className={classNames(classes.textField, classes.dense)}
                    margin="dense"
                    value={this.props.projectleadNaamValue}
                    maxLength="255"
                    onChange={this.props.onProjectleadNaamChange}
                    InputProps={{
                        classes: {
                        input: classes.resize,
                        },
                    }}
                    error={
                        this.props.pjleadError === true ? 
                        true:false
                    }
                    />
                </div>
                <div className="col s6">
                    <TextField
                    id="standard-dense"
                    name="pjemail"
                    label="Project Lead - e-mail adres"
                    className={classNames(classes.textField, classes.dense)}
                    margin="dense"
                    value={this.props.projectleadEmailValue}
                    maxLength="255"
                    onChange={this.props.onProjectleadEmailChange}
                    helperText={this.props.pjleadEmailError === true  && this.props.projectleadEmailValue.length > 0? "Enkel een Rijks e-mailadres is toegestaan" : ''}
                    InputProps={{
                        classes: {
                        input: classes.resize,
                        },
                    }}
                    error={
                        this.props.pjleadEmailError === true ? 
                        true:false}
                    />
                </div>
            </div>
        );
    };
};

//export
export default withStyles(styles)(Stap);

