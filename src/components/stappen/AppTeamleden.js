/**
 * Eerste stap van het formulier. Alle input velden zijn geimport van Material-UI.
 * Alle inputs hebben ook props values en pros onChange's. Om dit te kunnen aanpassen
 * graag kijken binnen AanvraagFormulier.js
 */

//imports 
import React from 'react';
import TextField from '@material-ui/core/TextField';
import AddIcon from '@material-ui/icons/Add';
import Minus from '@material-ui/icons/Delete';
import Button from '@material-ui/core/Button';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Titel from './AppFormTitle';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';

//CSS style
const styles = theme => ({
    container: {
    display: 'flex',
    flexWrap: 'wrap',
    },
    textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: '100%',
    paddingBottom: 35
    },
    dense: {
    marginTop: 19,
    },
    menu: {
    width: 200,
    },
    formControl: {
    margin: theme.spacing.unit,
    minWidth: 120,
    width: '100%'
    },
    selectEmpty: {
    marginTop: theme.spacing.unit * 2,
    },
    InputLabel:{
    fontSize: 15
    },
    Addbutton: {
    margin:8
    },
    button:{
      margin: theme.spacing.unit,
      marginRight: 10,
      margin:8
    },
    errorText:{
    color:'red',
    fontSize: 18
    },
    resize:{
      fontSize:13
    },
    switch:{
      marginTop:18
    },
    error: {
      backgroundColor: theme.palette.error.dark,
    },
    icon: {
      fontSize: 20,
    },
    iconVariant: {
      opacity: 0.9,
      marginRight: theme.spacing.unit,
    },
    message: {
      fontSize: 13,
      display: 'flex',
      alignItems: 'center',
    },
    marginBottom:{
      marginBottom:25
    }
});

/**
 * Teamleden is een dynamische array. Elke keer als de gebruiker drukt op de add knop, 
 * komt er een rij met tekstvelden in de array erbij. Verder kan een gebruiker met een switch
 * aangeven of de gebruiker adminrechten ja of nee heeft. Er moet altijd een teamlid mee gegeven worden 
 * anders werken de GraphQL functies ook niet. Let daar goed op. 
 */
class Teamleden extends React.Component {
    constructor(props){
      super(props);
    }

    moveButtonDown = () => {
      this.props.addRow();
      this.props.incrementIndex();
    }

    moveButtonUp = (index) => {
      this.props.removeRow(index);
      this.props.decrementIndex();
    }
    

    render(){
        const {classes} = this.props; 
        return (
        <React.Fragment>
          <Titel titel="Teamleden toevoegen"/>
          <div className="container">
                {this.props.rows.map((row, index) => (
                <div key={index}>
                  <div className="row">
                    <div className="col s2">
                    <FormControl component="fieldset" className={classes.switch}>
                        <FormLabel component="legend">Admin rechten</FormLabel>
                        <FormGroup>
                          <FormControlLabel
                            control={
                              <Switch
                                name="tlAdmin"
                                checked={row.tlAdmin}
                                onChange={e => this.props.onSelectChange(e, index)}
                                value="Admin rechten"
                              />
                            }
                          label={
                            row.tlAdmin === false ? "Nee" : "Ja"
                          }
                          />
                        </FormGroup>
                    </FormControl>
                    </div>
                    <div className="col s4">
                      <TextField
                          name="name"
                          id="standard-dense"
                          label="Naam"
                          margin="dense"
                          value={row.name}
                          error={row.errorNaam}
                          maxLength="255"
                          InputProps={{
                              classes: {
                              input: classes.resize,
                              },
                          }}
                          onChange={e => this.props.onChange(e, index)}
                          className={classNames(classes.textField, classes.dense)}
                      />
                    </div>
                    <div className="col s4">
                      <TextField
                          name="email"
                          id="standard-dense"
                          label="E-mail"
                          margin="dense"
                          value={row.email}
                          error={row.errorEmail}
                          helperText={row.errorEmail === true && row.email > 0 ? "Enkel een Rijks e-mailadres is toegestaan" : ''}
                          maxLength="255"
                          InputProps={{
                              classes: {
                              input: classes.resize,
                              },
                          }}
                          onChange={e => this.props.onChange(e, index)}
                          className={classNames(classes.textField, classes.dense)}
                      />
                    </div>
                      {index > 0 ? 
                        <div className="col s1">
                        <Button 
                              mini
                              className={classes.button}
                              variant="fab" 
                              color="primary"
                              aria-label="minus" 
                              onClick={e =>
                              this.moveButtonUp(index)}
                              data-toggle="tooltip"
                              className="btn btn-xs btn-primary"
                              >
                              <Minus/>
                          </Button> 
                        </div>: ''}
                        {index === this.props.rowIndex ?  <div className="col s1">
                          <Button 
                            mini
                            className={classes.button} 
                            variant="fab" 
                            color="primary" 
                            aria-label="Add" 
                            onClick={() => this.moveButtonDown()}
                            data-toggle="tooltip"
                            className="btn btn-xs btn-primary"
                          >
                            <AddIcon/>
                          </Button>
                        </div>:''}
                </div>
                </div>
                ))}
                </div>
        </React.Fragment>
    );
    }
};

//exports 
export default withStyles(styles)(Teamleden);
