/**
 * Agile portal, Een webportal ontwikkeld door IT4IT&Testmanagement
 * Copyright (C) 2018, SSC-ICT
 * mailto: agile@minbzk.nl
 */

 //imports
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import Input from '@material-ui/core/Input';

//CSS styles
const styles = theme => ({
      selectEmpty: {
        marginTop: theme.spacing.unit * 2,
        width: '100%'
      }
  });

/**
 * Er worden verschillende afdelingen gerenderd op basis van welke divisie is gekozen. 
 * Dit wordt gedaan met een simpel switch en case functie. 
 * Kan misschien verbeterd worden, nu namelijk veel regels code. 
 * @param {*} props 
 */
const Afdeling = (props) =>  {
    const {classes} = props;
    switch (props.value) {
            case "Services":   
                return  <div>
                            <InputLabel htmlFor="Afdeling">Afdeling</InputLabel>
                            <Select
                                    value="Ok"
                                    input={<Input name="Team" id="age-label-placeholder" />}
                                    
                                    name="afdeling"
                                    className={classes.selectEmpty}
                                    value={props.SelectAfdelingValue}
                                    onChange={props.handleAfdelingChange}
                                >
                                <MenuItem value="Klantmanagement">Klantmanagement</MenuItem>
                                <MenuItem value="Gebruikersondersteuning">Gebruikersondersteuning</MenuItem>
                                <MenuItem value="Servicestrategie">Servicestrategie</MenuItem>
                                <MenuItem value="IT-Office">IT-Office</MenuItem>
                                <MenuItem value="Projecten">Projecten</MenuItem>
                            </Select>
                           
                        </div>;
            case "Applicaties": 
                return <div>
                                <InputLabel htmlFor="Divisie">Afdeling</InputLabel>
                                <Select
                                    value="Ok"
                                    input={<Input name="Team" id="age-label-placeholder" />}
                                    
                                    name="afdeling"
                                    className={classes.selectEmpty}
                                    value={props.SelectAfdelingValue}
                                    onChange={props.handleAfdelingChange}
                                >
                            <MenuItem value="ERP">ERP</MenuItem>
                            <MenuItem value="ECM & Webapplicaties">ECM & Webapplicaties</MenuItem>
                            <MenuItem value="BI&I">BI&I</MenuItem>
                            </Select>
                            
                        </div>;
            case "Infrastructuur":  
                return <div>
                <InputLabel htmlFor="Divisie">Afdeling</InputLabel>
                <Select
                                    value="Ok"
                                    input={<Input name="Team" id="age-label-placeholder" />}
                                    
                                    name="afdeling"
                                    className={classes.selectEmpty}
                                    value={props.SelectAfdelingValue}
                                    onChange={props.handleAfdelingChange}
                                >
                            <MenuItem value="Rijkswerkomgeving">Rijkswerkomgeving</MenuItem>
                            <MenuItem value="Collaboration">Collaboration</MenuItem>
                            <MenuItem value="Operations">Operations</MenuItem>
                            <MenuItem value="DataCenter&Network-Services">Data Center & Network Services</MenuItem>
                            <MenuItem value="Platform Services">Platform Services</MenuItem>
                            </Select>
                           
                        </div>;
            case "Bedrijfsvoering":  
                return <div>
                <InputLabel htmlFor="Divisie">Afdeling</InputLabel>
                 <Select
                                    value="Ok"
                                    input={<Input name="Team" id="age-label-placeholder" />}
                                    
                                    name="afdeling"
                                    className={classes.selectEmpty}
                                    value={props.SelectAfdelingValue}
                                    onChange={props.handleAfdelingChange}
                                >
                                <MenuItem value="Inkoop">Inkoop</MenuItem>
                                <MenuItem value="Financien & Control">Financien & Control</MenuItem>
                                <MenuItem value="Interne Ondersteuning">Interne Ondersteuning</MenuItem>
                                <MenuItem value="Resources & Communicatie">Resources & Communicatie</MenuItem>
                            </Select>
                           
                        </div>;
            default:      
                return <div>
                           <InputLabel htmlFor="Divisie">Kies eerst een divisie</InputLabel>
                           <Select
                                    value="Ok"
                                    input={<Input name="Team" id="age-label-placeholder" />}
                                    disabled
                                    name="afdeling"
                                    className={classes.selectEmpty}
                                    value={props.SelectAfdelingValue}
                                    onChange={props.handleAfdelingChange}
                                >
                            </Select>
                           
                        </div>;
        }
};

//export 
export default withStyles(styles)(Afdeling);