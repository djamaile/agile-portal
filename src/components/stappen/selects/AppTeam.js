/**
 * Agile portal, Een webportal ontwikkeld door IT4IT&Testmanagement
 * Copyright (C) 2018, SSC-ICT
 * mailto: agile@minbzk.nl
 */

//imports
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import Input from '@material-ui/core/Input';

//CCS styles
const styles = theme => ({
      selectEmpty: {
        marginTop: theme.spacing.unit * 2,
        width: '100%'
      }
  });

/**
     * Er worden verschillende teams gerenderd op basis van welke divisie is afdeling. 
     * Dit wordt gedaan met een simpel switch en case functie. 
     * Kan misschien verbeterd worden, nu namelijk veel regels code. 
     * @param {*} props 
 */
const Team = (props) =>  {
    const {classes} = props;
    switch (props.value) {
            case "Klantmanagement":   
                return  <div>
                                <InputLabel htmlFor="Team">Team</InputLabel>
                                <Select
                                    value="Ok"
                                    input={<Input name="Team" id="age-label-placeholder" />}
                                    name="team"
                                    className={classes.selectEmpty}
                                    value={props.SelectTeamValue}
                                    onChange={props.handleTeamChange}
                                >
                                <MenuItem value="Relatiemanagement">Relatiemanagement</MenuItem>
                                <MenuItem value="Servicelevelmanagement">Service Level Management</MenuItem>
                            </Select>
                           
                        </div>;
            case "IT-Office": 
                return <div>
                            <InputLabel htmlFor="Team">Team</InputLabel>
                                <Select
                                    value="Ok"
                                    input={<Input name="Team" id="age-label-placeholder" />}
                                    name="team"
                                    className={classes.selectEmpty}
                                    value={props.SelectTeamValue}
                                    onChange={props.handleTeamChange}
                                >
                                <MenuItem value="Proces & Informatie">Proces & Informatie</MenuItem>
                                <MenuItem value="Innovatie">Innovatie</MenuItem>
                                <MenuItem value="Security">Security</MenuItem>
                                <MenuItem value="Architectuur">Architectuur</MenuItem>
                            </Select>
                            
                        </div>;
            case "Gebruikersondersteuning":  
                return <div>
                            <InputLabel htmlFor="Team">Team</InputLabel>
                                <Select
                                    value="Ok"
                                    input={<Input name="Team" id="age-label-placeholder" />}
                                    name="team"
                                    className={classes.selectEmpty}
                                    value={props.SelectTeamValue}
                                    onChange={props.handleTeamChange}
                                >
                                <MenuItem value="Helpdesk 1">Helpdesk 1</MenuItem>
                                <MenuItem value="Helpdesk 2">Helpdesk 2</MenuItem>
                                <MenuItem value="Locatie ond 1">Locatie ond 1</MenuItem>
                                <MenuItem value="Locatie ond 2">Locatie ond 2</MenuItem>
                                <MenuItem value="GCRS">Gebruikers-communicatie Regie & Specials</MenuItem>
                            </Select>
                           
                        </div>;
            case "Servicestrategie":  
                return <div>
                           <InputLabel htmlFor="Team">Team</InputLabel>
                                <Select
                                    value="Ok"
                                    input={<Input name="Team" id="age-label-placeholder" />}                               
                                    name="team"
                                    className={classes.selectEmpty}
                                    value={props.SelectTeamValue}
                                    onChange={props.handleTeamChange}
                                >
                                <MenuItem value="SEHH">Service-eigenaar Housing & Housting</MenuItem>
                                <MenuItem value="SEA">Service-eigenaar Applicaties</MenuItem>
                                <MenuItem value="SELS">Service-eigenaar Locatiegebonden Services</MenuItem>
                                <MenuItem value="SER">Service-eigenaar Rijkswerkomgeving</MenuItem>
                            </Select>
                           
                        </div>;
             case "Projecten":  
             return <div>
                         <InputLabel htmlFor="Team">Team</InputLabel>
                             <Select
                                 value="Ok"
                                 input={<Input name="Team" id="age-label-placeholder" />}                             
                                 name="team"
                                 className={classes.selectEmpty}
                                 value={props.SelectTeamValue}
                                 onChange={props.handleTeamChange}
                             >
                             <MenuItem value="Applicaties">Applicaties</MenuItem>
                             <MenuItem value="Locatie gebonden Services">Locatie gebonden Services</MenuItem>
                             <MenuItem value="Werkomgeving">Werkomgeving</MenuItem>
                             <MenuItem value="PMO">PMO</MenuItem>
                             <MenuItem value="T&T">T&T</MenuItem>
                         </Select>
                        
                     </div>;
                case "ERP":  
                return <div>
                           <InputLabel htmlFor="Team">Team</InputLabel>
                                <Select
                                    value="Ok"
                                    input={<Input name="Team" id="age-label-placeholder" />}                                
                                    name="team"
                                    className={classes.selectEmpty}
                                    value={props.SelectTeamValue}
                                    onChange={props.handleTeamChange}
                                >
                                <MenuItem value="FAB">Team Oracle FAB</MenuItem>
                                <MenuItem value="TAB">Team Oracle TAB</MenuItem>
                                <MenuItem value="SAP">Team SAP</MenuItem>
                            </Select>
                        </div>;
                case "ECM & Webapplicaties":  
                return <div>
                           <InputLabel htmlFor="Team">Team</InputLabel>
                                <Select
                                    value="Ok"
                                    input={<Input name="Team" id="age-label-placeholder" />}                                
                                    name="team"
                                    className={classes.selectEmpty}
                                    value={props.SelectTeamValue}
                                    onChange={props.handleTeamChange}
                                >
                                <MenuItem value="ECM">ECM</MenuItem>
                                <MenuItem value="ECM Filenet">ECM Filenet</MenuItem>
                                <MenuItem value="Beheer Webapplicaties">Beheer Webapplicaties</MenuItem>
                                <MenuItem value="Ontwikkeling Webapplicaties">Ontwikkeling Webapplicaties</MenuItem>
                            </Select>
                           
                        </div>;
                case "BI&I":  
                return <div>
                           <InputLabel htmlFor="Team">Team</InputLabel>
                                <Select
                                    value="Ok"
                                    input={<Input name="Team" id="age-label-placeholder" />}
                                    
                                    name="team"
                                    className={classes.selectEmpty}
                                    value={props.SelectTeamValue}
                                    onChange={props.handleTeamChange}
                                >
                                <MenuItem value="Beheer VK">Beheer VK</MenuItem>
                                <MenuItem value="IT4IT & Testmanagement">IT4IT & Testmanagement</MenuItem>
                                <MenuItem value="Identity en Acces Management">Identity en Acces Management</MenuItem>
                                <MenuItem value="IDM">IDM</MenuItem>
                            </Select> 
                        </div>;
                case "Rijkswerkomgeving":  
                return <div>
                            <InputLabel htmlFor="Team">Team</InputLabel>
                                <Select
                                    value="Ok"
                                    input={<Input name="Team" id="age-label-placeholder" />}
                                    
                                    name="team"
                                    className={classes.selectEmpty}
                                    value={props.SelectTeamValue}
                                    onChange={props.handleTeamChange}
                                >
                                <MenuItem value="Ops Dev">Ops Dev</MenuItem>
                                <MenuItem value="Packaging">Packaging</MenuItem>
                                <MenuItem value="Dev">Dev</MenuItem>
                                <MenuItem value="Specials">Specials</MenuItem>
                            </Select> 
                        </div>;
                case "Collaboration":  
                return <div>
                           <InputLabel htmlFor="Team">Team</InputLabel>
                                <Select
                                    value="Ok"
                                    input={<Input name="Team" id="age-label-placeholder" />}
                                    
                                    name="team"
                                    className={classes.selectEmpty}
                                    value={props.SelectTeamValue}
                                    onChange={props.handleTeamChange}
                                >
                               <InputLabel htmlFor="Team">Team</InputLabel>
                                <MenuItem value="UC">Unified Communications</MenuItem>
                                <MenuItem value="MS">Messaging Services</MenuItem>
                            </Select>
                           
                        </div>;
                case "Operations":  
                return <div>
                            <InputLabel htmlFor="Team">Team</InputLabel>
                                <Select
                                    value="Ok"
                                    input={<Input name="Team" id="age-label-placeholder" />}
                                    
                                    name="team"
                                    className={classes.selectEmpty}
                                    value={props.SelectTeamValue}
                                    onChange={props.handleTeamChange}
                                >
                                <MenuItem value="Delivery Services">Delivery Services</MenuItem>
                                <MenuItem value="Operations Control Room">Operations Control Room</MenuItem>
                                <MenuItem value="Operational Support">Operational Support</MenuItem>
                            </Select>
                           
                        </div>;
                case "DataCenter&Network-Services":  
                return <div>
                           <InputLabel htmlFor="Team">Team</InputLabel>
                                <Select
                                    value="Ok"
                                    input={<Input name="Team" id="age-label-placeholder" />}
                                    
                                    name="team"
                                    className={classes.selectEmpty}
                                    value={props.SelectTeamValue}
                                    onChange={props.handleTeamChange}
                                >
                                <MenuItem value="Campus Network Services">Campus Network Services</MenuItem>
                                <MenuItem value="Data Center Network Services">Data Center Network Services</MenuItem>
                                <MenuItem value="Network Development">Network Development</MenuItem>
                                <MenuItem value="Environmental Services">Environmental Services</MenuItem>
                                <MenuItem value="Logistiek">Logistiek</MenuItem>
                            </Select>
                           
                        </div>;
                case "Platform Services":  
                return <div>
                            <InputLabel htmlFor="Team">Team</InputLabel>
                                <Select
                                    value="Ok"
                                    input={<Input name="Team" id="age-label-placeholder" />}
                                    
                                    name="team"
                                    className={classes.selectEmpty}
                                    value={props.SelectTeamValue}
                                    onChange={props.handleTeamChange}
                                >
                                <MenuItem value="Cloud services">Cloud services</MenuItem>
                                <MenuItem value="Storage & Backup">Storage & Backup</MenuItem>
                                <MenuItem value="Database Services">Database Services</MenuItem>
                                <MenuItem value="Web & Applicatie Hosting">Web & Applicatie Hosting</MenuItem>
                            </Select>
                           
                        </div>;
                case "Inkoop":  
                return <div>
                          <InputLabel htmlFor="Team">Team</InputLabel>
                                <Select
                                    value="Ok"
                                    input={<Input name="Team" id="age-label-placeholder" />}
                                    
                                    name="team"
                                    className={classes.selectEmpty}
                                    value={props.SelectTeamValue}
                                    onChange={props.handleTeamChange}
                                >
                                <MenuItem value="Aanbesteden">Aanbesteden</MenuItem>
                                <MenuItem value="Administratie">Administratie</MenuItem>
                                <MenuItem value="Licentiedesk">Licentiedesk</MenuItem>
                            </Select>
                           
                        </div>;
                    case "Financien & Control":  
                    return <div>
                               <InputLabel htmlFor="Team">Team</InputLabel>
                                    <Select
                                        value="Ok"
                                        input={<Input name="Team" id="age-label-placeholder" />}
                                        
                                        name="team"
                                        className={classes.selectEmpty}
                                        value={props.SelectTeamValue}
                                        onChange={props.handleTeamChange}
                                    >
                                    <MenuItem value="Business Control">Business Control</MenuItem>
                                    <MenuItem value="Financial Control">Financial Control</MenuItem>
                                    <MenuItem value="Kwaliteit">Kwaliteit</MenuItem>
                                    <MenuItem value="Strategie en beleid">Strategie en beleid</MenuItem>
                                </Select>
                               
                            </div>;
                    case "Interne Ondersteuning":  
                    return <div>
                               <InputLabel htmlFor="Team">Team</InputLabel>
                                    <Select
                                        value="Ok"
                                        input={<Input name="Team" id="age-label-placeholder" />}
                                        
                                        name="team"
                                        className={classes.selectEmpty}
                                        value={props.SelectTeamValue}
                                        onChange={props.handleTeamChange}
                                    >
                                    <MenuItem value="Management Ondersteuning">Management Ondersteuning</MenuItem>
                                    <MenuItem value="Bedrijfsvoering Ondersteuning">Bedrijfsvoering Ondersteuning</MenuItem>
                                    <MenuItem value="Programma DIV">Programma DIV</MenuItem>
                                </Select>
                               
                            </div>;
                    case "Resources & Communicatie":  
                    return <div>
                               <InputLabel htmlFor="Team">Team</InputLabel>
                                    <Select
                                        value="Ok"
                                        input={<Input name="Team" id="age-label-placeholder" />}
                                        
                                        name="team"
                                        className={classes.selectEmpty}
                                        value={props.SelectTeamValue}
                                        onChange={props.handleTeamChange}
                                    >
                                    <MenuItem value="HR">HR</MenuItem>
                                    <MenuItem value="Communicatie">Communicatie</MenuItem>
                                    <MenuItem value="Trainees">Trainees</MenuItem>
                                </Select>  
                            </div>;
                default:      
                    return <div>
                               <InputLabel htmlFor="Team">Kies eerst afdeling</InputLabel>
                                    <Select
                                        value="Ok"
                                        input={<Input name="Team" id="age-label-placeholder" />}
                                        disabled
                                        name="team"
                                        className={classes.selectEmpty}
                                        value={props.SelectTeamValue}
                                        onChange={props.handleTeamChange}
                                    >
                                </Select>
                            </div>;
        }
};

//export
export default withStyles(styles)(Team);