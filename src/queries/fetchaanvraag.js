import gql from 'graphql-tag';

export default gql`
query{
  aanvragen{
    id
    datum
    naam
    divisie
    status
    verwerkt_door
    verwerkt_op
    project{
      naam
    }
  }
}
`;