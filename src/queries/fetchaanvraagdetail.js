import gql from 'graphql-tag';

export default gql`
    query AanvraagDetail($id:ID!){
    aanvraag(id: $id){
      id
      naam
      email
      divisie
      afdeling
      team
      status
      verwerkt_door
      verwerkt_op
      project {
        naam
        lead_naam
        lead_email
        type
        teamlid{
          naam
          email
          adminrechten
        }
      }
    }
}`;