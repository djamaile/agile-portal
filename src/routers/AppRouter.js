import React from 'react';
import {Router, Route, Switch} from 'react-router-dom';
import Dashboard from '../components/AppDashboard';
import Aanvraag from '../components/AppAanvraag';
import Header from '../components/AppHeader';
import Footer from '../components/AppFooter';
import Help from '../components/AppHelp';
import NotFound from '../components/AppNotfound';
import Jira from '../components/ci-cd-straat-products/Jira/AppJira';
import AanvraagDetail from '../components/admin/AppAanvraagDetail';
import history from '../data/history';
import Account from '../components/account-aanvraag/AppAcountAanvraag';
import Admin from '../components/admin/AppAdminDashboard';


const AppRouter = () =>( 
    <Router history={history}>
    <div>
        <Header/>
        <div className="main-container">
            <Switch>
                <Route path="/" component={Dashboard} exact={true} />
                <Route path="/jira" component={Jira}/>
                <Route path="/project-aanvraag" component={Aanvraag}/>
                <Route path="/account-aanvraag" component={Account}/>
                <Route path="/aanvraag/:id" component={AanvraagDetail} exact={true}/>
                <Route path="/Help" component={Help}/>
                <Route path="/admin" component={Admin}/>
                <Route component={NotFound}/>
            </Switch>
            <Footer/>
        </div> 
    </div>
    </Router>
);


export default AppRouter;
