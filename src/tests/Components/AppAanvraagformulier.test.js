import React from 'react';
import { shallow } from 'enzyme';
import AanvraagFormulier from '../../components/AanvraagFormulier';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({
    adapter: new Adapter()
});

test("Moet een correcte formulier renderen", () => {
    const wrapper = shallow(<AanvraagFormulier/>);
    expect(wrapper).toMatchSnapshot();
});
