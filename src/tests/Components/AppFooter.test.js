import React from 'react';
import { shallow } from 'enzyme';
import Dashboard from '../../components/AppFooter';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({
    adapter: new Adapter()
});

test("Moet dashboard pagina goed renderen", () => {
    const wrapper = shallow(<Dashboard/>);
    expect(wrapper).toMatchSnapshot();
});