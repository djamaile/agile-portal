import React from 'react';
import { shallow } from 'enzyme';
import {handelEmailError} from '../../components/AanvraagFormulier';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({
    adapter: new Adapter()
});

test("Moet false returnen", () => {
    const result = handelEmailError("email@gmail.com", "error");
    expect(result).toEqual(false);
})
